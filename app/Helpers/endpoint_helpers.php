<?php 

function auth_endpoint($module,$path){
	if(empty($module)){
		$url = sprintf('%s%s/%s',env('ENDPOINT_APIGATEWAY_BASEPATH'),env('ENDPOINT_APIGATEWAY_AUTH'),$path);
	}
	else{
		$url = sprintf('%s%s/%s/%s',env('ENDPOINT_APIGATEWAY_BASEPATH'),env('ENDPOINT_APIGATEWAY_AUTH'),$module,$path);
	}
    return $url;
}


function users_endpoint($module,$path){
	if(empty($module)){
		$url = sprintf('%s%s/%s',env('ENDPOINT_APIGATEWAY_BASEPATH'),env('ENDPOINT_APIGATEWAY_USERS'),$path);
	}
	else{
		$url = sprintf('%s%s/%s/%s',env('ENDPOINT_APIGATEWAY_BASEPATH'),env('ENDPOINT_APIGATEWAY_USERS'),$module,$path);
	}
    return $url;
}