<?php

namespace App\Guards;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Contracts\Auth\UserProvider;


use Exception;

class OAuthGuard{

    protected $request;
    protected $guzzle;
    protected $access_token;
    protected $refresh_token;

    public function __construct(UserProvider $provider,$request)
    {
        $this->guzzle        = new Client();
        $this->request       = $request;
        $this->access_token  = $request->cookie('access_token');
        $this->refresh_token = $request->cookie('refresh_token');
    }

    public function attempt($username,$password){
        try{
            $result = $this->guzzle->post(auth_endpoint('auth','login'), [
                'form_params'=>[
                    'client_id'     =>config('app.client_id'),
                    'client_secret' =>config('app.client_secret'),
                    'form_params'   =>[
                        'username'      =>$username,
                        'password'      =>$password,
                    ]
                ],
            ]);
            $result = json_decode($result->getBody()->getContents(),true);
            $this->setAccessToken(array_get($result,'access_token'));
            $this->setRefreshToken(array_get($result,'refresh_token'));
            return true;
        }
        catch(RequestException $e){
            if(env('APP_DEBUG')){
                $errors = json_decode($e->getResponse()->getBody()->getContents(),true);
                \Log::warning('AUTH_ATTEMPT',[$errors]);
            }
        }
    }    

    public function check(){
        try{     
            $result = $this->guzzle->get(auth_endpoint('auth','check'), [
                'query' => [
                    "access_token"  =>$this->getAccessToken(),
                    "refresh_token" =>$this->getRefreshToken(),
                    'client_id'     =>config('app.client_id'),
                    'client_secret' =>config('app.client_secret'),
                ]
            ]);
            return true;
        }
        catch(RequestException $e){
            if(env('APP_DEBUG')){
                $errors = json_decode($e->getResponse()->getBody()->getContents(),true);
                \Log::warning('AUTH_CHECK',[$errors]);
            }
            return false;
        }
    }

    public function user(){
        try{     
            $result = $this->guzzle->get(auth_endpoint('auth','user'), [
                'query' => [
                    "access_token"  =>$this->getAccessToken(),
                    "refresh_token" =>$this->getRefreshToken(),
                    'client_id'     =>config('app.client_id'),
                    'client_secret' =>config('app.client_secret'),
                ]
            ]);
            $result = json_decode($result->getBody()->getContents(),true);
            array_forget($result,'access_token');
            array_forget($result,'refresh_token');
            return $result;

        }
        catch(RequestException $e){
            if(env('APP_DEBUG')){
                $errors = json_decode($e->getResponse()->getBody()->getContents(),true);
                \Log::warning('AUTH_USER',[$errors]);
            }
            return false;
        }
    }

    public function logout(){
        try{
            $result = $this->guzzle->get(auth_endpoint('auth','logout'), [
                'query'=>[
                    'access_token'  =>$this->getAccessToken(),
                    'client_id'     =>config('app.client_id'),
                    'client_secret' =>config('app.client_secret'),
                ],
            ]);
            return true;
        }
        catch(RequestException $e){
            if(env('APP_DEBUG')){
                $errors = json_decode($e->getResponse()->getBody()->getContents(),true);
                \Log::warning('AUTH_LOGOUT',[$errors]);
            }
            return false;
        }
    }

    public function refresh(){
        try{
            $result = $this->guzzle->get(auth_endpoint('auth','refresh'), [
                'query'=>[
                    'refresh_token' =>$this->getRefreshToken(),
                    'client_id'     =>config('app.client_id'),
                    'client_secret' =>config('app.client_secret'),
                ]
            ]);
            $result = json_decode($result->getBody()->getContents(),true);

            $this->setAccessToken(array_get($result,'access_token'));
            $this->setRefreshToken(array_get($result,'refresh_token'));

            return true;
        }
        catch(RequestException $e){
            if(env('APP_DEBUG')){
                $errors = json_decode($e->getResponse()->getBody()->getContents(),true);
                \Log::warning('AUTH_REFRESH',[$errors]);
            }
            return $e->getResponse();
        }
    }

    public function getAccessToken(){
        return $this->access_token;
    }

    public function getRefreshToken(){
        return $this->refresh_token;
    }

    public function setAccessToken($access_token){
        $this->access_token = $access_token;
        return $this->access_token;
    }

    public function setRefreshToken($refresh_token){
        $this->refresh_token = $refresh_token;
        return $this->refresh_token;
    }

}