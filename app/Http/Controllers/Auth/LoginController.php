<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cookie;
use Closure;
use Artisan;
use Session;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
        // $this->middleware('authnbi')->except('logout');
    }

    public function Login(){
        $auth = Auth::guard('api');

        if($auth->check()===true){
            return redirect('/');
        }
        return view('login/index');
    }

    public function Authenticate(Request $request){
        $auth = Auth::guard('api');

        if($auth->attempt($request->username, $request->password)===true){
            $user = $auth->user();
            $role_id = $user['role_id'];
            $rolecount = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id])->count();
            if($rolecount > 0){
                return redirect('/')
            ->cookie('access_token', $auth->getAccessToken(), env('ACCESS_TOKEN_COOKIE_LIFE'))
            ->cookie('refresh_token', $auth->getRefreshToken(), env('REFRESH_TOKEN_COOKIE_LIFE'));
            }
            else{
                $error = true;
                return view('login/index')->with(['error'=>$error]);
            }
            
        }else{
            $error = true;
            return view('login/index')->with(['error'=>$error]);
        }
    }

    public function Logout(Request $request){
        $auth = Auth::guard('api');
        $auth->logout();
        Artisan::call('cache:clear');
        Session::flush();
        return redirect('login');
    }
    
}
