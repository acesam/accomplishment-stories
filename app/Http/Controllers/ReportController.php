<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cookie;
use Closure;
use DB;

class ReportController extends Controller
{
    /**
     * Display a listing of all agent scores.
     *
     * @return \Illuminate\Http\Response
     */

    public function Index(){
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        return view('index')->with(['role_id'=>$role_id]);
    }

    public function AllAgents()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>1])->count();
        if($check_role == 0){
            return view("no-access");
        }

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','0', '0', '0', '0', '0']);
        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $selected_agentrole = 0;

        return view("allagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole]);
    }

    public function FilterAllAgents(Request $request)
    {
        $min =  date('Y-m-d', strtotime($request->min));
        $max = date('Y-m-d', strtotime($request->max));
        $selected_casetype = $request->casetype;
        $selected_agenttype = $request->agenttype;
        $selected_agentrole = $request->agentrole;

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', [$selected_casetype,$min,$max,$selected_agenttype,$selected_agentrole, '0' , '0' , '0', '0']);
        

        return view("allagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole]);
    }

    public function ServiceAgents()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>2])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $services = DB::table('acmo_settingsdb.settings_caseservices')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','0', 'eec66846-a653-4096-98dc-15069ed55c4c' , '0', '0', '0']);
        
        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_service = "eec66846-a653-4096-98dc-15069ed55c4c";

        return view("serviceagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "services"=>$services, "selected_service"=>$selected_service]);
    }

    public function FilterServiceAgents(Request $request)
    {
        $min =  date('Y-m-d', strtotime($request->min));
        $max = date('Y-m-d', strtotime($request->max));
        $selected_casetype = $request->casetype;
        $selected_agenttype = $request->agenttype;
        $selected_agentrole = $request->agentrole;
        $selected_service = $request->service;

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $services = DB::table('acmo_settingsdb.settings_caseservices')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', [$selected_casetype,$min,$max,$selected_agenttype,$selected_agentrole, $selected_service, '0', '0', '0']);

        return view("serviceagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "services"=>$services, "selected_service"=>$selected_service]);
    }

    public function DivisionAgents()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>3])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','0', '0', 'c610dde9-8640-477e-9f8f-746c5aa37c62', '0', '0']);

        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_division = "c610dde9-8640-477e-9f8f-746c5aa37c62";

        return view("divisionagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "divisions"=>$divisions, "selected_division"=>$selected_division]);
    }

    public function FilterDivisionAgents(Request $request)
    {
        $min =  date('Y-m-d', strtotime($request->min));
        $max = date('Y-m-d', strtotime($request->max));
        $selected_casetype = $request->casetype;
        $selected_agenttype = $request->agenttype;
        $selected_agentrole = $request->agentrole;
        $selected_division = $request->division;

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', [$selected_casetype,$min,$max,$selected_agenttype,$selected_agentrole, '0', $selected_division, '0', '0']);

        return view("divisionagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "divisions"=>$divisions, "selected_division"=>$selected_division]);
    }

    public function RegionAgents()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>4])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $regions = DB::table('acmo_settingsdb.settings_regions')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','0', '0', '0', '%', '0']);

        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_region = "0";

        return view("regionagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "regions"=>$regions, "selected_region"=>$selected_region]);
    }

    public function FilterRegionAgents(Request $request)
    {
        $min =  date('Y-m-d', strtotime($request->min));
        $max = date('Y-m-d', strtotime($request->max));
        $selected_casetype = $request->casetype;
        $selected_agenttype = $request->agenttype;
        $selected_agentrole = $request->agentrole;
        $selected_region = $request->region == 0 ? '%': $request->region;

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $regions = DB::table('acmo_settingsdb.settings_regions')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', [$selected_casetype,$min,$max,$selected_agenttype,$selected_agentrole, '0', '0', $selected_region, '0']);

        return view("regionagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "regions"=>$regions, "selected_region"=>$selected_region]);
    }

    public function DistrictAgents()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>5])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $districts = DB::table('acmo_settingsdb.settings_districtoffices')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','0', '0', '0', '0', '%']);

        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_district = "0";

        return view("districtagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "districts"=>$districts, "selected_district"=>$selected_district]);
    }

    public function FilterDistrictAgents(Request $request)
    {
        $min =  date('Y-m-d', strtotime($request->min));
        $max = date('Y-m-d', strtotime($request->max));
        $selected_casetype = $request->casetype;
        $selected_agenttype = $request->agenttype;
        $selected_agentrole = $request->agentrole;
        $selected_district = $request->district == 0 ? '%': $request->district;

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $districts = DB::table('acmo_settingsdb.settings_districtoffices')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', [$selected_casetype,$min,$max,$selected_agenttype,$selected_agentrole, '0', '0', '0', $selected_district]);

        return view("districtagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "districts"=>$districts, "selected_district"=>$selected_district]);
    }

    public function CurrentCases($id, $type, $min, $max, $agent)
    {   
        $user = DB::table('acmo_usersdb.user_profiles')->where(["user_id"=>$id]);
        $scores = "";
        if($agent == "main"){
            $scores = DB::select('CALL acmo_reportsdb.REPORTS_AGENTS_SCORES(?, ?, ?, ?)', [$type, $id, $min, $max]);
        }else{
            $scores = DB::select('CALL acmo_reportsdb.REPORTS_AGENTS_SCORES_TAGS(?, ?, ?, ?)', [$type, $id, $min, $max]);
        }
        


        return view("scores")->with(["scores"=>$scores, "user"=>$user]);
    }

    public function ServiceRankingAgents()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>7])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $services = DB::table('acmo_settingsdb.settings_caseservices')->where(['deleted_at'=>NULL])->get();
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();
        $regions = DB::table('acmo_settingsdb.settings_regions')->where(['deleted_at'=>NULL])->get();
        $districts = DB::table('acmo_settingsdb.settings_districtoffices')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_RANKING_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','123e4ab4-8fe6-44f1-a398-42f772d9a252', 'eec66846-a653-4096-98dc-15069ed55c4c' , '0', '0', '0','2016-01-01',date("Y-m-d")]);
        
        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $pmin = "2016-01-01";
        $pmax = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_service = "eec66846-a653-4096-98dc-15069ed55c4c";

        return view("servicerankingagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "pmin"=>$pmin, "pmax"=>$pmax, "selected_agentrole"=>$selected_agentrole, "services"=>$services, "selected_service"=>$selected_service]);
    }

    public function FilterServiceRankingAgents(Request $request)
    {
        $min =  date('Y-m-d', strtotime($request->min));
        $max = date('Y-m-d', strtotime($request->max));
        $pmin =  date('Y-m-d', strtotime($request->pmin));
        $pmax = date('Y-m-d', strtotime($request->pmax));
        $selected_casetype = $request->casetype;
        $selected_agenttype = $request->agenttype;
        $selected_agentrole = $request->agentrole;
        $selected_service = $request->service;

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $services = DB::table('acmo_settingsdb.settings_caseservices')->where(['deleted_at'=>NULL])->get();
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();
        $regions = DB::table('acmo_settingsdb.settings_regions')->where(['deleted_at'=>NULL])->get();
        $districts = DB::table('acmo_settingsdb.settings_districtoffices')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_RANKING_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$selected_casetype,$min,$max,$selected_agenttype,$selected_agentrole, $selected_service, '0', '0', '0', $pmin, $pmax]);
        $newagents = array();

        return view("servicerankingagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "pmin"=>$pmin, "pmax"=>$pmax, "selected_agentrole"=>$selected_agentrole, "services"=>$services,"selected_service"=>$selected_service]);
    }

    public function AllRankingAgents()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>6])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $services = DB::table('acmo_settingsdb.settings_caseservices')->where(['deleted_at'=>NULL])->get();
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();
        $regions = DB::table('acmo_settingsdb.settings_regions')->where(['deleted_at'=>NULL])->get();
        $districts = DB::table('acmo_settingsdb.settings_districtoffices')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_RANKING_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','0', '0' , '0', '0', '0','2016-01-01',date("Y-m-d")]);
        
        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $pmin = "2016-01-01";
        $pmax = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_service = "eec66846-a653-4096-98dc-15069ed55c4c";

        return view("allrankingagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "pmin"=>$pmin, "pmax"=>$pmax, "selected_agentrole"=>$selected_agentrole, "services"=>$services]);
    }

    public function FilterAllRankingAgents(Request $request)
    {
        $min =  date('Y-m-d', strtotime($request->min));
        $max = date('Y-m-d', strtotime($request->max));
        $pmin =  date('Y-m-d', strtotime($request->pmin));
        $pmax = date('Y-m-d', strtotime($request->pmax));
        $selected_casetype = $request->casetype;
        $selected_agenttype = $request->agenttype;
        $selected_agentrole = $request->agentrole;

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $services = DB::table('acmo_settingsdb.settings_caseservices')->where(['deleted_at'=>NULL])->get();
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();
        $regions = DB::table('acmo_settingsdb.settings_regions')->where(['deleted_at'=>NULL])->get();
        $districts = DB::table('acmo_settingsdb.settings_districtoffices')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_RANKING_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$selected_casetype,$min,$max,$selected_agenttype,$selected_agentrole, '0', '0', '0', '0', $pmin, $pmax]);
        $newagents = array();

        return view("allrankingagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "pmin"=>$pmin, "pmax"=>$pmax, "selected_agentrole"=>$selected_agentrole, "divisions"=>$divisions]);
    }

    public function DivisionRankingAgents()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>8])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_RANKING_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','123e4ab4-8fe6-44f1-a398-42f772d9a252', '0' , '0', '0', '0','2016-01-01',date("Y-m-d")]);
        
        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $pmin = "2016-01-01";
        $pmax = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_division = "c610dde9-8640-477e-9f8f-746c5aa37c62";

        $newagents = array();
        foreach($agents as $agent){
            if($agent->division_id == $selected_division){
                array_push($newagents, $agent);
            }
        }

        return view("divisionrankingagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "pmin"=>$pmin, "pmax"=>$pmax, "selected_agentrole"=>$selected_agentrole, "divisions"=>$divisions, "selected_division"=>$selected_division]);
    }

    public function FilterDivisionRankingAgents(Request $request)
    {
        $min =  date('Y-m-d', strtotime($request->min));
        $max = date('Y-m-d', strtotime($request->max));
        $pmin =  date('Y-m-d', strtotime($request->pmin));
        $pmax = date('Y-m-d', strtotime($request->pmax));
        $selected_casetype = $request->casetype;
        $selected_agenttype = $request->agenttype;
        $selected_agentrole = $request->agentrole;
        $selected_division = $request->division;

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_RANKING_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$selected_casetype,$min,$max,$selected_agenttype,$selected_agentrole, '0', '0', '0', '0', $pmin, $pmax]);
        $newagents = array();
        foreach($agents as $agent){
            if($agent->division_id == $selected_division){
                array_push($newagents, $agent);
            }
        }
        $agents = $newagents;

        return view("divisionrankingagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "pmin"=>$pmin, "pmax"=>$pmax, "selected_agentrole"=>$selected_agentrole, "divisions"=>$divisions, "selected_division"=>$selected_division]);
    }

    public function RegionRankingAgents()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>9])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $regions = DB::table('acmo_settingsdb.settings_regions')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_RANKING_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','123e4ab4-8fe6-44f1-a398-42f772d9a252', '0' , '0', '0', '0','2016-01-01',date("Y-m-d")]);

        
        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $pmin = "2016-01-01";
        $pmax = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_region = "0";
        $newagents = array();
        foreach($agents as $agent){
            if($agent->region_id != "" && $agent->district_id == ""){
                array_push($newagents, $agent);
            }
        }
        $agents = $newagents;

        return view("regionrankingagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "pmin"=>$pmin, "pmax"=>$pmax, "selected_agentrole"=>$selected_agentrole, "regions"=>$regions, "selected_region"=>$selected_region]);
    }

    public function FilterRegionRankingAgents(Request $request)
    {
        $min =  date('Y-m-d', strtotime($request->min));
        $max = date('Y-m-d', strtotime($request->max));
        $pmin =  date('Y-m-d', strtotime($request->pmin));
        $pmax = date('Y-m-d', strtotime($request->pmax));
        $selected_casetype = $request->casetype;
        $selected_agenttype = $request->agenttype;
        $selected_agentrole = $request->agentrole;
        $selected_region = $request->region;

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $regions = DB::table('acmo_settingsdb.settings_regions')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_RANKING_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$selected_casetype,$min,$max,$selected_agenttype,$selected_agentrole, '0', '0', '0', '0', $pmin, $pmax]);
        $newagents = array();

        if($selected_region == 0){
            foreach($agents as $agent){
                if($agent->region_id != "" && $agent->district_id == ""){
                    array_push($newagents, $agent);
                }
            }
        }else{
            foreach($agents as $agent){
                if($agent->region_id == $selected_region && $agent->district_id == ""){
                    array_push($newagents, $agent);
                }
            }
        }
        $agents = $newagents;

        return view("regionrankingagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "pmin"=>$pmin, "pmax"=>$pmax, "selected_agentrole"=>$selected_agentrole, "regions"=>$regions, "selected_region"=>$selected_region]);
    }

    public function DistrictRankingAgents()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>10])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $districts = DB::table('acmo_settingsdb.settings_districtoffices')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_RANKING_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','123e4ab4-8fe6-44f1-a398-42f772d9a252', '0' , '0', '0', '0','2016-01-01',date("Y-m-d")]);
        
        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $pmin = "2016-01-01";
        $pmax = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_district = "0";

        $newagents = array();

        foreach($agents as $agent){
            if($agent->district_id != ""){
                array_push($newagents, $agent);
            }
        }
        $agents = $newagents;

        return view("districtrankingagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "pmin"=>$pmin, "pmax"=>$pmax, "selected_agentrole"=>$selected_agentrole, "districts"=>$districts, "selected_district"=>$selected_district]);
    }

    public function FilterDistrictRankingAgents(Request $request)
    {
        $min =  date('Y-m-d', strtotime($request->min));
        $max = date('Y-m-d', strtotime($request->max));
        $pmin =  date('Y-m-d', strtotime($request->pmin));
        $pmax = date('Y-m-d', strtotime($request->pmax));
        $selected_casetype = $request->casetype;
        $selected_agenttype = $request->agenttype;
        $selected_agentrole = $request->agentrole;
        $selected_district = $request->district;

        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $districts = DB::table('acmo_settingsdb.settings_districtoffices')->where(['deleted_at'=>NULL])->get();

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_RANKING_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$selected_casetype,$min,$max,$selected_agenttype,$selected_agentrole, '0', '0', '0', '0', $pmin, $pmax]);
        $newagents = array();

        foreach($agents as $agent){
            if($agent->district_id == $selected_district){
                array_push($newagents, $agent);
            }
        }
        $agents = $newagents;

        return view("districtrankingagents")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "pmin"=>$pmin, "pmax"=>$pmax, "selected_agentrole"=>$selected_agentrole, "districts"=>$districts, "selected_district"=>$selected_district]);
    }

    public function NatureOfCases()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>11])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $services = DB::table('acmo_settingsdb.settings_caseservices')->where(['deleted_at'=>NULL])->get();
        $natures = DB::table('acmo_settingsdb.settings_casenatures')->where(['deleted_at'=>NULL])->orderBy('name','ASC')->get();
        $nature_count = (DB::table('acmo_settingsdb.settings_casenatures')->where(['deleted_at'=>NULL])->count() * 3) + 2;
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();
        //->where(['caseservice_id'=>'eec66846-a653-4096-98dc-15069ed55c4c'])

        $agents = DB::select('CALL acmo_reportsdb.REPORTS_ALL_AGENTS(?, ?, ?, ?, ?, ?, ?, ?, ?)', ['0','2016-01-01',date("Y-m-d"),'main','0', 'eec66846-a653-4096-98dc-15069ed55c4c' , '0', '0', '0']);
        $index = 0;
        // set_time_limit(0);
        // DB::table('acmo_reportsdb.natures_cases_count')->truncate();
        // foreach($natures as $nature){
        //     foreach($divisions as $division){
        //         DB::select('CALL acmo_reportsdb.REPORTS_NATURECASES_COUNT(?, ?, ?)', [$nature->casenature_id, $division->division_id, $division->name]);
        //     }
        // }

        $data = DB::table('acmo_reportsdb.natures_cases_count')->get();

        $selected_casetype = 0;
        $selected_agenttype = "main";
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_service = "eec66846-a653-4096-98dc-15069ed55c4c";

        return view("naturecases")->with(["allagents"=>$agents, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "selected_agenttype"=>$selected_agenttype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "services"=>$services, "selected_service"=>$selected_service, "natures"=>$natures, "columns"=>$nature_count, 'divisions'=>$divisions, 'datas'=>$data]);
    }

    public function NatureOfCasesList($division, $nature, $status)
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>11])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $division_id = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->where(['name'=>$division])->value('division_id');
        $cases = DB::table('acmo_reportsdb.reports_cases_natures')->where(['casenature_id'=>$nature, 'division_id'=>$division_id, 'casestatus_id'=>$status])->get();
        
        return view("naturelist")->with(["cases"=>$cases]);
    }

    public function NatureOfCasesTotal($division, $status)
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>11])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $division_id = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->where(['name'=>$division])->value('division_id');
        $cases = DB::table('acmo_reportsdb.reports_cases_natures')->where(['division_id'=>$division_id, 'casestatus_id'=>$status])->get();
        
        return view("naturetotal")->with(["cases"=>$cases]);
    }

    public function NatureOfCasesSelector()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>11])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $natures = DB::table('acmo_settingsdb.settings_casenatures')->where(['deleted_at'=>NULL])->orderBy('name','ASC')->get();
        
        return view("natureselector")->with(["natures"=>$natures]);
    }

    public function NatureOfCasesSingle($casenature_id)
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>11])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $services = DB::table('acmo_settingsdb.settings_caseservices')->where(['deleted_at'=>NULL])->get();
        $naturename = DB::table('acmo_settingsdb.settings_casenatures')->where(['deleted_at'=>NULL])->where(['casenature_id'=>$casenature_id])->value('name');

        $selected_casetype = 0;
        $min = "2016-01-01";
        $max = date("Y-m-d");
        $selected_agentrole = 0;
        $selected_service = 0;
        
        return view("naturesingle")->with(["divisions"=>$divisions, "casenature_id"=>$casenature_id, "naturename"=>$naturename, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "services"=>$services, "selected_service"=>$selected_service]);
    }

    public function FilterNatureOfCasesSingle(Request $request, $casenature_id)
    {
        $divisions = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->get();
        $casetypes = DB::table('acmo_settingsdb.settings_casecategories')->where(['deleted_at'=>NULL])->get();
        $services = DB::table('acmo_settingsdb.settings_caseservices')->where(['deleted_at'=>NULL])->get();
        $naturename = DB::table('acmo_settingsdb.settings_casenatures')->where(['deleted_at'=>NULL])->where(['casenature_id'=>$casenature_id])->value('name');

        $selected_casetype = $request->casetype;
        $min = $request->min;
        $max = $request->max;
        $selected_agentrole = $request->agentrole;
        $selected_service = $request->service;
        
        return view("naturesingle")->with(["divisions"=>$divisions, "casenature_id"=>$casenature_id, "naturename"=>$naturename, "casetypes"=>$casetypes, "selected_casetype"=>$selected_casetype, "min"=>$min, "max"=>$max, "selected_agentrole"=>$selected_agentrole, "services"=>$services, "selected_service"=>$selected_service]);
    }

    public function NatureOfCasesListSingle($division, $nature, $status, $service, $category, $min, $max, $role)
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        $check_role = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role_id, 'report_type'=>11])->count();
        if($check_role == 0){
            return view("no-access");
        }
        $division_id = DB::table('acmo_settingsdb.settings_divisions')->where(['deleted_at'=>NULL])->where(['name'=>$division])->value('division_id');
        $cases = DB::table('acmo_reportsdb.reports_cases_natures')->where(['casenature_id'=>$nature, 'division_id'=>$division_id, 'casestatus_id'=>$status])->whereBetween('created_date', [$min, $max]);
        if($category != 0){
            $cases = $cases->where(['casecategory_id'=>$category]);
        }

        if($role != 0){
            $cases = $cases->where(['role_id'=>$role]);
        }

        if($service != 0){
            $cases = $cases->where(['caseservice_id'=>$service]);
        }

        $cases = $cases->get();
        
        return view("naturelist")->with(["cases"=>$cases]);
    }

}
