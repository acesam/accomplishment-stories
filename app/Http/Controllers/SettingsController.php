<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cookie;
use Closure;
use DB;

class SettingsController extends Controller
{
    
    public function Roles()
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        if($role_id != "0e061de9-597c-48fe-8953-c03751cd5f70"){
            return redirect('/');
        }

        $roles = DB::table('acmo_usersdb.roles_main')->get();
        $access_roles = DB::table('acmo_reportsdb.reports_roles')->get();
        return view('settings/roles')->with(['roles'=>$roles, 'access_roles'=>$access_roles]);
    }

    public function SaveRoles(Request $request)
    {
        $auth = Auth::guard('api');
        $user = $auth->user();
        $role_id = $user['role_id'];
        if($role_id != "0e061de9-597c-48fe-8953-c03751cd5f70"){
            return redirect('/');
        }
        $roles1 = $request->roles1;
        $roles2 = $request->roles2;
        $roles3 = $request->roles3;
        $roles4 = $request->roles4;
        $roles5 = $request->roles5;
        $roles6 = $request->roles6;
        $roles7 = $request->roles7;
        $roles8 = $request->roles8;
        $roles9 = $request->roles9;
        $roles10 = $request->roles10;
        $roles11 = $request->roles11;
        DB::table('acmo_reportsdb.reports_roles')->truncate();

        for($i = 1; $i < 12; $i++){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>"0e061de9-597c-48fe-8953-c03751cd5f70", "report_type"=>$i]);
        }
        if(!empty($roles1))
        foreach($roles1 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>1]);
        }

        if(!empty($roles2))
        foreach($roles2 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>2]);
        }

        if(!empty($roles3))
        foreach($roles3 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>3]);
        }

        if(!empty($roles4))
        foreach($roles4 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>4]);
        }

        if(!empty($roles5))
        foreach($roles5 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>5]);
        }

        if(!empty($roles6))
        foreach($roles6 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>6]);
        }

        if(!empty($roles7))
        foreach($roles7 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>7]);
        }

        if(!empty($roles8))
        foreach($roles8 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>8]);
        }

        if(!empty($roles9))
        foreach($roles9 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>9]);
        }

        if(!empty($roles10))
        foreach($roles10 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>10]);
        }

        if(!empty($roles11))
        foreach($roles11 as $form_role){
            DB::table('acmo_reportsdb.reports_roles')->insert(["role_id"=>$form_role, "report_type"=>11]);
        }
        $roles = DB::table('acmo_usersdb.roles_main')->get();
        $access_roles = DB::table('acmo_reportsdb.reports_roles')->get();
        return view('settings/roles')->with(['roles'=>$roles, 'access_roles'=>$access_roles, 'success'=>true]);
    }
}
