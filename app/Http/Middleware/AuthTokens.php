<?php

namespace App\Http\Middleware;
use App\Models\Users\AuthenticationClass;
use Cookie;
use Closure;
use Illuminate\Support\Facades\Auth;

class AuthTokens
{

    public function handle($request, Closure $next)
    {
        $auth           = Auth::guard('api');
        $user           = session()->get('user');
        $access_token   = session()->get('access_token');
        $refresh_token  = session()->get('refresh_token');

        if(!empty($user) && !empty($access_token) && !empty($refresh_token)){
            $request->merge(session()->all());
            session()->regenerate();
            return $this->next($next($request),$access_token,$refresh_token);
        }
        else if($auth->check()){
            $user           = $auth->user();
            $access_token   = $auth->getAccessToken();
            $refresh_token  = $auth->getRefreshToken();
            $web_key        = env('APP_KEY','');

            $data = [
                'author'        =>$user['full_name'],
                'user'          =>$user,
                'user_id'       =>$user['user_id'],
                'access_token'  =>$access_token,
                'refresh_token' =>$refresh_token,
            ];

            session()->put('web_key',$web_key);

            foreach($data as $key=>$value){
                session()->put($key,$value);
            }
            $request->merge($data);
            return $this->next($next($request),$access_token,$refresh_token);
            
        }
        else{
            if($auth->refresh()===true){
                $access_token   = $auth->getAccessToken();
                $refresh_token  = $auth->getRefreshToken();
                
                return $this->next(redirect(url()->current())->withInput(),$access_token,$refresh_token);
            }
            return redirect('/login');            
        }
    }

    private function next($next,$access_token,$refresh_token){
        return $next
		    ->cookie('access_token',$access_token,config('app.access_token_cookie_life'))
		    ->cookie('refresh_token',$refresh_token,env('app.refresh_token_cookie_life'));

    }
}
