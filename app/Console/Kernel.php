<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        set_time_limit(0);
        $schedule->call(function () {
            DB::select('CALL acmo_reportsdb.GENERATE_REPORTS_CASES()');
        })->everyThirtyMinutes();

        $schedule->call(function () {
            $natures = DB::table('acmo_settingsdb.settings_casenatures')->orderBy('name','ASC')->get();
            foreach($natures as $nature)
            {
                DB::select('CALL acmo_reportsdb.REPORTS_NATURECASES_COUNT(?)', [$nature->casenature_id]);
            }
        })->everyThirtyMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
