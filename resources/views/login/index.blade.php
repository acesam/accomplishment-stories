<!DOCTYPE html>
<html>
<head>
    <title>Report Login</title>
    <style type="text/css">
        body{
            padding-top: 7%;
        }
        form{
            width: 25%;
            margin: 0 auto;
            border: 1px solid #b1b1b1;
            padding: 10px 40px 50px 40px;
            border-top: 2px solid #db2828!important;
        }
        input[type=text], input[type=password]{
            width: 95%;
            height: 35px;
            padding: 0px 10px;
        }
        div{
            padding: 15px 0px;
        }
        input[type="submit"]{
            background-color: #b21e1e;
            color: #fff;
            text-shadow: none;
            border: none;
            padding: 10px 35px;
            float:right;
        }
        img{
            margin: 0 auto;
            display: block;
            width: 65%;
        }
        p{
            font-size: 11px;
            color: #6b6b6b;
            font-family: Lato;
            text-align: center;
        }
        .error{
            font-size: 14px;
            font-weight: bold;
            color: red;
            float: left;
            margin: 5px;
        }
    </style>
</head>
<body>
<form action="" method="post">
    @csrf
    <div>
        <img src="http://10.100.100.1:1180/images/top_menu_logo.jpg">
        <hr>
        <p>WELCOME GUEST! PLEASE LOGIN</p>
    </div>
    <div>
        <input type="text" name="username" placeholder="Enter Username" />
    </div>
    <div>
        <input type="password" name="password" placeholder="Enter Password" />
    </div>
    <div>
        @if(isset($error))
        <p class="error">Access denied!</p>
        @endif
        <input type="submit" value="Login" />
    </div>
</form>
</body>
</html>