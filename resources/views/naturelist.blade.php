@include('includes.header')
    <div class="container">
        <div class="row mt10">
            <table id="agentslist" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Case ID</th>
                        <th>CCN ID</th>
                        <th>User ID</th>
                        <th>Date Created</th>
                        <th>Case Nature</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($cases as $case)
                <tr>
                    <td>{{ $case->case_id }}</td>
                    <td>{{ $case->ccn_id }}</td>
                    <td>{{ $case->user_id }}</td>
                    <td>{{ $case->created_date }}</td>
                    <td>{{ $case->casenature_name }}</td>

                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <p id="dateprinted"><strong>Date Printed:</strong> {{ date("m-d-Y") }}</p>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4" id="prepared">
                <p>System Administrator</p>
                <p><strong>Prepared By</strong></p>
            </div>
            <div class="col-md-4 offset-md-4 col-sm-4 offset-sm-4" id="approved">
                <p>&nbsp;</p>
                <p><strong>Approved By</strong></p>
            </div>
        </div>
    </div>
    </body>
    <script src="/js/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#agentslist').DataTable( {
                dom: 'lBfrtip',
                "lengthMenu": [[50, 100, 500, 1000, -1], [50, 100, 500, 1000, "All"]],
                buttons: [
                'copyHtml5',
                'excelHtml5'],
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false
                
            });

            $('#min').keyup( function() { table.draw(); } );
            $('#max').keyup( function() { table.draw(); } );

            $( "#min" ).datepicker();
            $( "#max" ).datepicker();

            $('#submit').click(function(){
                 $( "#datefilter" ).submit();
            });

        } );

        
    </script>
</html>
