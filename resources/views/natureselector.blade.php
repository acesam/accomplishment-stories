<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Accomplishment Reports</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">

        <!-- Styles -->
        <style>
            select{
                padding: 10px;
                margin-bottom: 20px;
            }

            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 48px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div id="top_menu" class="ui fixed top menu">
            <div class="item"><img class="ui image" src="http://10.100.100.1:1180/images/top_menu_logo.jpg"></div>
            <div class="ui icon labeled top pointing dropdown link item"><a href="https://acmo.nbi.gov.ph"><span>Back to main site</span></a></div>
            <div class="right icon menu">
                <div class="ui icon labeled top dropdown link item"><a href="/">Home</a></div>
                <div class="ui icon labeled top dropdown link item"><a href="/settings/roles">Logout</a></div>
            </div>
        </div>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Nature of Cases
                </div>
                <select id="reports">
                    <option value="/agents/naturecases">All</option>
                @foreach($natures as $nature)
                    <option value="/agents/naturecases/{{ $nature->casenature_id }}">{{ $nature->name }}</option>
                @endforeach
                </select>
                <br>
                <button class="print_button">Generate</button>
            </div>
        </div>
        <script src="/js/jquery-1.12.4.js"></script>
        <script type="text/javascript">
            $(".print_button").click(function(){
                var link = $("#reports").find(":selected").val()
                window.open(link, '_self')
            });
        </script>
    </body>
</html>
