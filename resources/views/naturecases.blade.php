<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Accomplishment Report Stories</title>

        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
        <!-- <link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap4.min.css"/> -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="/css/style.css">
       
    </head>
    <body>
    <div id="top_menu" class="ui fixed top menu">
            <div class="item"><img class="ui image" src="http://10.100.100.1:1180/images/top_menu_logo.jpg"></div>
            <div class="ui icon labeled top pointing dropdown link item"><a href="https://acmo.nbi.gov.ph"><span>Back to main site</span></a></div>
            <div class="right icon menu">
                <div class="ui icon labeled top dropdown link item"><a href="/">Home</a></div>
                <div class="ui icon labeled top dropdown link item"><a href="/logout">Logout</a></div>
            </div>
        </div>
    <div class="header">
        <h5>Department of Justice</h3>
        <h3>National Bureau of Investigation</h3>
        <h3>Accomplishment Report</h3>
        <div id="print">
            <button class="print_button" onClick="window.print()">Print Report</button>
        </div>
    </div>
    <div class="container">
        <!-- <form id="datefilter" action="" method="post">
        @csrf
            <div class="search-bar">
                <div class="row mt10">
                    <div class="col-lg-2">
                        <div class="m0a">
                            <label>Case Service: </label><br />
                            <select class="form-control form-control-sm casetype" name="service">
                                @foreach($services as $service)
                                    <option value="{{ $service->caseservice_id }}" {{ $service->caseservice_id === $selected_service ? "selected" : "" }}>{{ $service->name }} ({{$service->description}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="m0a">
                            <label>Category: </label><br />
                            <select class="form-control form-control-sm casetype" name="casetype">
                                <option value="0" {{ "0" === $selected_casetype ? "selected" : "" }}>All</option>
                                @foreach($casetypes as $casetype)
                                    <option value="{{ $casetype->casecategory_id }}" {{ $casetype->casecategory_id === $selected_casetype ? "selected" : "" }}>{{ $casetype->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="m0a date">
                            <label>Period: &nbsp;</label><br />
                            <input type="text" name="min" id="min" value="{{ $min }}" /> - <input type="text" name="max" id="max" value="{{ $max }}" />
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="m0a">
                            <label>Agent Type: </label><br />
                            <select class="form-control form-control-sm casetype" name="agenttype">
                                <option value="0" {{ "0" === $selected_agenttype ? "selected" : "" }}>All</option>
                                <option value="main" {{ "main" === $selected_agenttype ? "selected" : "" }}>Main Agent</option>
                                <option value="tag" {{ "tag" === $selected_agenttype ? "selected" : "" }}>Tag Agent</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="m0a">
                        <label>Agent Role: </label><br />
                            <select class="form-control form-control-sm casetype" name="agentrole">
                                <option value="123e4ab4-8fe6-44f1-a398-42f772d9a252" {{ "123e4ab4-8fe6-44f1-a398-42f772d9a252" === $selected_agentrole ? "selected" : "" }}>Agent</option>
                                <option value="b700d769-6bf3-4628-8544-a73bc7fd322b" {{ "b700d769-6bf3-4628-8544-a73bc7fd322b" === $selected_agentrole ? "selected" : "" }}>Special Investigator</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 center mt25">
                        <button  class="button_search searchbtn"> Search </button>
                    </div>
                </div>
            </div>
        </form> -->
        <div class="row mt10">
            <div class="col-md-6">
                <p id="tag"><strong>Bureau</strong></p>
            </div>
            <div class="col-md-6 tright">
                <p>Period: As of: {{ $min }} - {{ $max }}</p>
            </div>
            <table id="agentslist" class="table table-bordered" style="width:100%">
                <thead>
                    <tr><td colspan="3">Breakdown of Cases</td></tr>
                    <tr><td colspan="1">Service: Investigative Service</td><td colspan="2">For the period of: 01/01/2016 - today</td></tr>
                    <tr><td colspan="3">Nature of Cases</td></tr>
                    <tr>
                        <th></th>          
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($natures as $nature)
                    <tr>
                        <td><strong>{{ $nature->name }}</strong></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Unit/Division</th>          
                        <th>Prosection</th>
                        <th>Closure</th>
                    </tr>
                    @php

                        $currentdata = DB::table('acmo_reportsdb.natures_cases_count')->where(['casenature_id'=>$nature->casenature_id])->get();
                    @endphp
                    @foreach($currentdata as $data)

                    <tr>
                        <td>{{ $data->division }}</td>
                        <td><a href="/agents/naturecaseslist/{{$data->division}}/{{$nature->casenature_id}}/CS4">{{ $data->prosecution }}</a></td>
                        <td><a href="/agents/naturecaseslist/{{$data->division}}/{{$nature->casenature_id}}/CS0">{{ $data->closed }}</a></td>
                    </tr>
                    @endforeach
                @endforeach 
                    <tr>
                        <td><strong>Unit/Division</strong></td>
                        <td><strong>Points From Previous</strong></td>
                        <td><strong>Grand Total</strong></td>
                    </tr>
                    @foreach($divisions as $division)
                    @php
                        $total = DB::table('acmo_reportsdb.natures_cases_count')->select(DB::raw('sum(prosecution) as prosecution, sum(closed) as closed'))->where(['division'=>$division->name])->get();
                    @endphp
                    <tr>
                        <td>{{ $division->name }}</td>
                        <td><a href="/agents/naturecasestotal/{{$data->division}}/CS4">{{ $total[0]->prosecution }}</a></td>
                        <td><a href="/agents/naturecasestotal/{{$data->division}}/CS0">{{ $total[0]->closed }}</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <p id="dateprinted"><strong>Date Printed:</strong> {{ date("m-d-Y") }}</p>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4" id="prepared">
                <p>&nbsp;</p>
                <p><strong>Prepared By</strong></p>
            </div>
            <div class="col-md-4 offset-md-4 col-sm-4 offset-sm-4" id="approved">
                <p>&nbsp;</p>
                <p><strong>Approved By</strong></p>
            </div>
        </div>
    </div>
    </body>
    <script src="/js/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#agentslist').DataTable( {
                dom: 'lBfrtip',
                buttons: [
                'copyHtml5',
                'excelHtml5'],
                "paging": false,
                "ordering": false,
                "searching": false,
                "bInfo" : false
                
            });

            $('#min').keyup( function() { table.draw(); } );
            $('#max').keyup( function() { table.draw(); } );

            $( "#min" ).datepicker({ dateFormat: 'yy-mm-dd' }).on("show", function() {
                $(this).val("{{ $min }}").datepicker('update');
            });
            
            $( "#max" ).datepicker({ dateFormat: 'yy-mm-dd' }).on("show", function() {
                $(this).val("{{ $max }}").datepicker('update');
            });

            $('#submit').click(function(){
                 $( "#datefilter" ).submit();
            });

        } );

        
    </script>
</html>
