<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Accomplishment Reports</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">

        <!-- Styles -->
        <style>
            select{
                padding: 10px;
                margin-bottom: 20px;
            }

            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 90vh;
                margin: 0;
            }

            .full-height {
                height: 85vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 48px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .links{
                margin-top: 33px;
                text-align: right
            }
            .links a{
                margin-top:20px;
            }
        </style>
    </head>
    <body>
    	<div id="top_menu" class="ui fixed top menu">
    		<div class="item"><img class="ui image" src="http://10.100.100.1:1180/images/top_menu_logo.jpg"></div>
    		<div class="ui icon labeled top pointing dropdown link item"><a href="https://acmo.nbi.gov.ph"><span>Back to main site</span></a></div>
    		<div class="right icon menu">
    			@if($role_id == "0e061de9-597c-48fe-8953-c03751cd5f70")
    			<div class="ui icon labeled top dropdown link item"><a href="/settings/roles">Manage Role Permission</a></div>
    			@endif
    			<div class="ui icon labeled top dropdown link item"><a href="/logout">Logout</a></div>
    		</div>
    	</div>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Accomplishment Reports
                </div>
                <select id="reports">
                    <optgroup label="Accomplishment reports">
                        <option value="/agents/all">Bureau-Wide</option>
                        <option value="/agents/services">Service</option>
                        <option value="/agents/division">Division Agents</option>
                        <option value="/agents/region">Region Agents</option>
                        <option value="/agents/district">District Agents</option>
                    </optgroup>
                    <optgroup label="Agent ranking">
                        <option value="/agents/ranking/all">All Agents Ranking</option>
                        <option value="/agents/ranking/services">Agents Under Services Ranking</option>
                        <option value="/agents/ranking/division">Division Agents Ranking</option>
                        <option value="/agents/ranking/region">Region Agents Ranking</option>
                        <option value="/agents/ranking/district">District Agents Ranking</option>
                    </optgroup>
                    <optgroup label="Case Natures">
                        <option value="/agents/naturecases/select">Nature of Cases (Single)</option>
                    </optgroup>
                </select>
                <br>
                <button class="print_button">Generate</button>
                <br>
                
                    
            </div>
        </div>
        <script src="/js/jquery-1.12.4.js"></script>
        <script type="text/javascript">
            $(".print_button").click(function(){
                var link = $("#reports").find(":selected").val()
                window.open(link, "_self")
            });
        </script>
    </body>
</html>
