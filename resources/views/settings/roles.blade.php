<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Accomplishment Reports</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">

        <!-- Styles -->
        <style>
            select{
                padding: 10px;
                margin-bottom: 20px;
            }

            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 90vh;
                margin: 0;
            }

            .full-height {
                height: 85vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 48px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .links{
                margin-top: 33px;
                text-align: right
            }
            .links a{
                margin-top:20px;
            }

            table{
            	margin: 0 auto;
            }
            td{
            	font-size:18px;
            	text-align: left;
            }
        </style>
    </head>
    <body>
        <div id="top_menu" class="ui fixed top menu">
            <div class="item"><img class="ui image" src="http://10.100.100.1:1180/images/top_menu_logo.jpg"></div>
            <div class="ui icon labeled top pointing dropdown link item"><a href="https://acmo.nbi.gov.ph"><span>Back to main site</span></a></div>
            <div class="right icon menu">
                <div class="ui icon labeled top dropdown link item"><a href="/">Home</a></div>
                <div class="ui icon labeled top dropdown link item"><a href="/logout">Logout</a></div>
            </div>
        </div>
        <div class="flex-center position-ref full-height">
            <div class="content">
            	@if(isset($success))
	            <div class="ui positive message">
			      <i class="close icon"></i>
			      <div class="header">
			        Success!
			      </div>
			      <p>Access Roles has been saved.</p>
			    </div>
			    @endif
            	<form action="" method="post" style="margin-top:200px">
            		@csrf
	                <div class="title m-b-md">
	                    Report Role Permissions
	                </div>
	                <div>
	                
	                	@foreach($roles as $role)
    	                	@if($role->role_id != "0e061de9-597c-48fe-8953-c03751cd5f70")
    		                		@php
    		                			$selected = "checked";
    		                			$report_count1 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>1])->count();
                                        $report_count2 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>2])->count();
                                        $report_count3 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>3])->count();
                                        $report_count4 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>4])->count();
                                        $report_count5 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>5])->count();
                                        $report_count6 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>6])->count();
                                        $report_count7 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>7])->count();
                                        $report_count8 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>8])->count();
                                        $report_count9 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>9])->count();
                                        $report_count10 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>10])->count();
                                        $report_count11 = DB::table('acmo_reportsdb.reports_roles')->where(['role_id'=>$role->role_id, 'report_type'=>11])->count();
    		                			
    		                		@endphp
                                    <div class="accordion_container"> 
                                        <div class="accordion">
                                            <i class="fas fa-angle-right" style="margin: 0px 10px"></i>
                                            {{$role->role_name}}
                                        </div>
                                        
            		                	<div class="panel">
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles1[]" value="{{$role->role_id}}" {{ $report_count1 > 0 ? $selected : "" }}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    Bureau-Wide
                                            </div>
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles2[]" value="{{$role->role_id}}" {{$report_count2 > 0 ? $selected : ""}}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    Service
                                            </div>
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles3[]" value="{{$role->role_id}}" {{$report_count3 > 0 ? $selected : ""}}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    Division Agents
                                            </div>
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles4[]" value="{{$role->role_id}}" {{$report_count4 > 0 ? $selected : ""}}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    Region Agents
                                            </div>
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles5[]" value="{{$role->role_id}}" {{$report_count5 > 0 ? $selected : ""}}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    District Agents
                                            </div>
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles6[]" value="{{$role->role_id}}" {{$report_count6 > 0 ? $selected : ""}}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    All Agent Ranking
                                            </div>
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles7[]" value="{{$role->role_id}}" {{$report_count7 > 0 ? $selected : ""}}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    Agents Under Service Ranking
                                            </div>
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles8[]" value="{{$role->role_id}}" {{$report_count8 > 0 ? $selected : ""}}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    Division Agents Ranking
                                            </div>
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles9[]" value="{{$role->role_id}}" {{$report_count9 > 0 ? $selected : ""}}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    Region Agents Ranking
                                            </div>
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles10[]" value="{{$role->role_id}}" {{$report_count10 > 0 ? $selected : ""}}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    District Agents Ranking
                                            </div>
                                            <div>
                                                    <label class="switch">
                                                      <input type="checkbox" name="roles11[]" value="{{$role->role_id}}" {{$report_count11 > 0 ? $selected : ""}}>
                                                      <span class="slider round"></span>
                                                    </label> 
                                                    Case Natures
                                            </div>
                                        </div>
                                    </div>
    	                	@endif
	                	@endforeach
	                </div>
	                <br>
	                <input type="submit" class="save_button" value="Save" />
                </form>
                <br>
                
                    
            </div>
        </div>
        <script src="/js/jquery-1.12.4.js"></script>
        <script type="text/javascript">
            $(".print_button").click(function(){
                var link = $("#reports").find(":selected").val()
                window.open(link, '_blank')
            });
            var acc = document.getElementsByClassName("accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
              acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight){
                  panel.style.maxHeight = null;
                } else {
                  panel.style.maxHeight = panel.scrollHeight + "px";
                } 
              });
            }
        </script>
    </body>
</html>
