@include('includes.header')
    <div class="container">
        <div class="row mt10">
            <table id="agentslist" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name of Agent</th>
                        <th>CCN</th>
                        <th>Date Approved</th>
                        <th>Status</th>
                        <th>Score</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($scores as $score)
                <tr>
                    <td>{{ $user->value("first_name") . " " . $user->value("last_name") }}</td>
                    <td>{{ $score->ccn_id }}</td>
                    <td>{{ $score->date_approved }}</td>
                    @php
                        if($score->casestatus_id == "CS0"){ $status = "Closed"; }
                        else if($score->casestatus_id == "CS4"){ $status = "Prosecuted"; }
                        else{ $status = "Active"; }
                    @endphp
                    <td>{{ $status }}</td>
                    <td>{{ $score->score }}</td>

                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <p id="dateprinted"><strong>Date Printed:</strong> {{ date("m-d-Y") }}</p>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4" id="prepared">
                <p>&nbsp;</p>
                <p><strong>Prepared By</strong></p>
            </div>
            <div class="col-md-4 offset-md-4 col-sm-4 offset-sm-4" id="approved">
                <p>&nbsp;</p>
                <p><strong>Approved By</strong></p>
            </div>
        </div>
    </div>
    </body>
    <script src="/js/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#agentslist').DataTable( {
                dom: 'lBfrtip',
                "lengthMenu": [[50, 100, 500, 1000, -1], [50, 100, 500, 1000, "All"]],
                buttons: [
                {
                    extend: 'copy',
                    text: 'Copy to clipboard',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                },
                'excelHtml5',
                'csvHtml5'],
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false
                
            });

            $('#min').keyup( function() { table.draw(); } );
            $('#max').keyup( function() { table.draw(); } );

            $( "#min" ).datepicker();
            $( "#max" ).datepicker();

            $('#submit').click(function(){
                 $( "#datefilter" ).submit();
            });

        } );

        
    </script>
</html>
