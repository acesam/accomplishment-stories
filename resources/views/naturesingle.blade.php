<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Accomplishment Report Stories</title>

        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
        <!-- <link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap4.min.css"/> -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="/css/style.css">
       
    </head>
    <body>
    <div id="top_menu" class="ui fixed top menu">
            <div class="item"><img class="ui image" src="http://10.100.100.1:1180/images/top_menu_logo.jpg"></div>
            <div class="ui icon labeled top pointing dropdown link item"><a href="https://acmo.nbi.gov.ph"><span>Back to main site</span></a></div>
            <div class="right icon menu">
                <div class="ui icon labeled top dropdown link item"><a href="/">Home</a></div>
                <div class="ui icon labeled top dropdown link item"><a href="/logout">Logout</a></div>
            </div>
        </div>
    <div class="header">
        <h5>Department of Justice</h3>
        <h3>National Bureau of Investigation</h3>
        <h3>Accomplishment Report</h3>
        <div id="print">
            <button class="print_button" onClick="window.print()">Print Report</button>
        </div>
    </div>
    <div class="container">
                <form id="datefilter" action="" method="post">
        @csrf
            <div class="search-bar">
                <div class="row mt10">
                    <div class="col-lg-3">
                        <div class="m0a">
                            <label>Case Service: </label><br />
                            <select class="form-control form-control-sm casetype" name="service">
                                    <option value="0">All Services</option>
                                @foreach($services as $service)
                                    <option value="{{ $service->caseservice_id }}" {{ $service->caseservice_id === $selected_service ? "selected" : "" }}>{{ $service->name }} ({{$service->description}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="m0a">
                            <label>Category: </label><br />
                            <select class="form-control form-control-sm casetype" name="casetype">
                                <option value="0" {{ "0" === $selected_casetype ? "selected" : "" }}>All</option>
                                @foreach($casetypes as $casetype)
                                    <option value="{{ $casetype->casecategory_id }}" {{ $casetype->casecategory_id === $selected_casetype ? "selected" : "" }}>{{ $casetype->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="m0a date">
                            <label>Period: &nbsp;</label><br />
                            <input type="text" name="min" id="min" value="{{ $min }}" /> - <input type="text" name="max" id="max" value="{{ $max }}" />
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="m0a">
                        <label>Agent Role: </label><br />
                            <select class="form-control form-control-sm casetype" name="agentrole">
                                <option value="0" {{ "0" === $selected_agentrole ? "selected" : "" }}>All</option>
                                <option value="123e4ab4-8fe6-44f1-a398-42f772d9a252" {{ "123e4ab4-8fe6-44f1-a398-42f772d9a252" === $selected_agentrole ? "selected" : "" }}>Agent</option>
                                <option value="b700d769-6bf3-4628-8544-a73bc7fd322b" {{ "b700d769-6bf3-4628-8544-a73bc7fd322b" === $selected_agentrole ? "selected" : "" }}>Special Investigator</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 center mt25">
                        <button  class="button_search searchbtn"> Search </button>
                    </div>
                </div>
            </div>
        </form>
        <div class="row mt10">
            <div class="col-md-6">
                <p id="tag"><strong>Bureau</strong></p>
            </div>
            <div class="col-md-6 tright">
                
            </div>
            <table id="agentslist" class="table table-bordered" style="width:100%">
                <thead>
                    <tr><td colspan="3">Breakdown of Cases</td></tr>
                    <tr><td colspan="1">Service: Investigative Service</td><td colspan="2">For the period of: 01/01/2016 - Today</td></tr>
                    <tr><td colspan="3">Nature of Cases</td></tr>
                    <tr>
                        <th></th>          
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td><strong>{{ $naturename }}</strong></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Unit/Division</th>          
                        <th>Prosection</th>
                        <th>Closure</th>
                    </tr>
                </thead>
                <tbody>
                    
                    @foreach($divisions as $division)

                    <tr>
                        <td>{{ $division->name }}</td>
                        @php

                        $prosecuted = DB::table('acmo_reportsdb.reports_cases_natures')->where(['casenature_id'=>$casenature_id, 'division_id'=>$division->division_id, 'casestatus_id'=>'CS4'])->whereBetween('created_date', [$min, $max]);
                        
                        $closed = DB::table('acmo_reportsdb.reports_cases_natures')->where(['casenature_id'=>$casenature_id, 'division_id'=>$division->division_id, 'casestatus_id'=>'CS0'])->whereBetween('created_date', [$min, $max]);

                        if($selected_casetype != 0){
                            $prosecuted = $prosecuted->where(['casecategory_id'=>$casetype->casecategory_id]);
                            $closed = $closed->where(['casecategory_id'=>$casetype->casecategory_id]);
                        }

                        if($selected_agentrole != 0){
                            $prosecuted = $prosecuted->where(['role_id'=>$selected_agentrole]);
                            $closed = $closed->where(['role_id'=>$selected_agentrole]);
                        }

                        if($selected_service != 0){
                            $prosecuted = $prosecuted->where(['caseservice_id'=>$service->caseservice_id]);
                            $closed = $closed->where(['caseservice_id'=>$service->caseservice_id]);
                        }

                        $prosecuted_count = $prosecuted->count();
                        $closed_count = $closed->count();
                        
                        @endphp
                        <td><a href="/agents/naturecaseslist/{{$division->name}}/{{$casenature_id}}/CS4/{{$selected_service}}/{{$selected_casetype}}/{{$min}}/{{$max}}/{{$selected_agentrole}}">{{ $prosecuted_count }}</a></td>
                        <td><a href="/agents/naturecaseslist/{{$division->name}}/{{$casenature_id}}/CS0/{{$selected_service}}/{{$selected_casetype}}/{{$min}}/{{$max}}/{{$selected_agentrole}}">{{ $closed_count }}</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <p id="dateprinted"><strong>Date Printed:</strong> {{ date("m-d-Y") }}</p>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4" id="prepared">
                <p>&nbsp;</p>
                <p><strong>Prepared By</strong></p>
            </div>
            <div class="col-md-4 offset-md-4 col-sm-4 offset-sm-4" id="approved">
                <p>&nbsp;</p>
                <p><strong>Approved By</strong></p>
            </div>
        </div>
    </div>
    </body>
    <script src="/js/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#agentslist').DataTable( {
                dom: 'lBfrtip',
                buttons: [
                {
                    extend: 'copy',
                    text: 'Copy to clipboard',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                },
                'excelHtml5'],
                "paging": false,
                "ordering": true,
                "searching": false,
                "bInfo" : false
                
            });

           $('#min').keyup( function() { table.draw(); } );
            $('#max').keyup( function() { table.draw(); } );

            $( "#min" ).datepicker({ dateFormat: 'yy-mm-dd' }).on("show", function() {
                $(this).val("{{ $min }}").datepicker('update');
            });
            
            $( "#max" ).datepicker({ dateFormat: 'yy-mm-dd' }).on("show", function() {
                $(this).val("{{ $max }}").datepicker('update');
            });

            $('#submit').click(function(){
                 $( "#datefilter" ).submit();
            });
        } );

        
    </script>
</html>
