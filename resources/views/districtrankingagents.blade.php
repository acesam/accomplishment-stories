@include('includes.rankingheader')
    <div class="container">
        <form id="datefilter" action="" method="post">
        @csrf
            <div class="search-bar">
                <div class="row mt10">
                    <div class="col-lg-2">
                        <div class="m0a">
                            <label>District: </label><br />
                            <select class="form-control form-control-sm casetype" id="districtselect" name="district">
                                <option value="0">All</option>
                                @foreach($districts as $district)
                                    <option value="{{ $district->districtoffice_id }}" {{ $district->districtoffice_id === $selected_district ? "selected" : "" }}>{{ $district->name }} ({{$district->description}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="m0a">
                            <label>Category: </label><br />
                            <select class="form-control form-control-sm casetype" name="casetype">
                                <option value="0" {{ "0" === $selected_casetype ? "selected" : "" }}>All</option>
                                @foreach($casetypes as $casetype)
                                    <option value="{{ $casetype->casecategory_id }}" {{ $casetype->casecategory_id === $selected_casetype ? "selected" : "" }}>{{ $casetype->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="m0a date">
                            <label>Period: &nbsp;</label><br />
                            <input type="text" name="min" id="min" value="{{ $min }}" /> - <input type="text" name="max" id="max" value="{{ $max }}" />
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="m0a">
                            <label>Agent Type: </label><br />
                            <select class="form-control form-control-sm casetype" name="agenttype">
                                <option value="0" {{ "0" === $selected_agenttype ? "selected" : "" }}>All</option>
                                <option value="main" {{ "main" === $selected_agenttype ? "selected" : "" }}>Main Agent</option>
                                <option value="tag" {{ "tag" === $selected_agenttype ? "selected" : "" }}>Tag Agent</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="m0a">
                        <label>Agent Role: </label><br />
                            <select class="form-control form-control-sm casetype" name="agentrole">
                                <option value="123e4ab4-8fe6-44f1-a398-42f772d9a252" {{ "123e4ab4-8fe6-44f1-a398-42f772d9a252" === $selected_agentrole ? "selected" : "" }}>Agent</option>
                                <option value="b700d769-6bf3-4628-8544-a73bc7fd322b" {{ "b700d769-6bf3-4628-8544-a73bc7fd322b" === $selected_agentrole ? "selected" : "" }}>Special Investigator</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="dateprevious">
                            <label>Previous Period: &nbsp;</label><br />
                            <input type="text" name="pmin" id="pmin" value="{{ $pmin }}" /> - <input type="text" name="pmax" id="pmax" value="{{ $pmax }}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 center mt25">
                        <button  class="button_search searchbtn"> <i class="fas fa-search"></i> Search </button>
                    </div>
                </div>
            </div>
        </form>
        <div class="row mt10">
            <div class="col-md-6 col-sm-6">
                <p id="tag"><strong>Bureau </strong></p>
            </div>
            <div class="col-md-6 col-sm-6 tright">
                <p>Period: As of: {{ $pmin }} - {{ $max }}</p>
            </div>

            <table id="agentslist" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th colspan="8" style="text-align: center;">Breakdown of Cases</th>
                    </tr>
                    <tr>
                        <th colspan="2" id="service"></th>
                        <th colspan="4">For the Period Of: {{ $min }} to {{ $max }}</th>
                        <th colspan="1">For the Period Of: {{ $pmin }} to {{ $pmax }}</th>
                        <th colspan="1"></th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Unit</th>
                        <th>Prosecution</th>
                        <th>Closure</th>
                        <th>Active Cases</th>
                        <th>Total</th>
                        <th>Points From Previous</th>
                        <th>Grand Total</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($allagents as $agent)
                <tr>
                   <td>{{ $agent->full_name }}</td>
                    @php
                        $unit = "";
                        if($agent->division_name != ""){ $unit = $agent->division_name; }
                        else if($agent->region_name != ""){ $unit = $agent->region_name; }
                        else{ $unit = $agent->district_name; }
                    @endphp
                    <td>{{ $unit }}</td>
                    @php
                        $newmin = date("Y-m-d", strtotime($min));
                        $newmax = date("Y-m-d", strtotime($max));
                    @endphp
                    <td><a href="/agents/cases/{{ $agent->user_id }}/prosecution/{{$newmin}}/{{$newmax}}/{{$selected_agenttype}}" target="_blank">{{ $agent->prosecuted }}</a></td>
                    <td><a href="/agents/cases/{{ $agent->user_id }}/closed/{{$newmin}}/{{$newmax}}/{{$selected_agenttype}}" target="_blank">{{ $agent->closed }}</a></td>
                    <td><a href="/agents/cases/{{ $agent->user_id }}/active/{{$newmin}}/{{$newmax}}/{{$selected_agenttype}}" target="_blank">{{ $agent->active }}</a></td>
                    <td><a href="/agents/cases/{{ $agent->user_id }}/all/{{$newmin}}/{{$newmax}}/{{$selected_agenttype}}" target="_blank">{{ $agent->total }}</a></td>
                    <td><a href="/agents/cases/{{ $agent->user_id }}/all/{{$pmin}}/{{$pmax}}/{{$selected_agenttype}}" target="_blank">{{ $agent->previous }}</a></td>
                    <td>{{ $agent->total + $agent->previous }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
@include('includes.rankingfooter')