<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Accomplishment Report Stories</title>

        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        
        <link rel="stylesheet" type="text/css" href="/css/style.css">
       
    </head>
    <body>
    <div id="top_menu" class="ui fixed top menu">
            <div class="item"><img class="ui image" src="http://10.100.100.1:1180/images/top_menu_logo.jpg"></div>
            <div class="ui icon labeled top pointing dropdown link item"><a href="https://acmo.nbi.gov.ph"><span>Back to main site</span></a></div>
            <div class="right icon menu">
                <div class="ui icon labeled top dropdown link item"><a href="/">Home</a></div>
                <div class="ui icon labeled top dropdown link item"><a href="/logout">Logout</a></div>
            </div>
        </div>
    <div class="header">
        <h5>Department of Justice</h3>
        <h3>National Bureau of Investigation</h3>
        <h3>Agent Ranking</h3>
        <div id="print">
            <button class="print_button" onClick="window.print()"><i class="fa fa-print" aria-hidden="true"></i> Print Report</button>
        </div>
    </div>