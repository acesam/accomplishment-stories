        <div class="row">
            <p id="dateprinted"><strong>Date Printed:</strong> {{ date("m-d-Y") }}</p>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4" id="prepared">
                <p>&nbsp;</p>
                <p><strong>Prepared By</strong></p>
            </div>
            <div class="col-md-4 offset-md-4 col-sm-4 offset-sm-3 col-xs-4 offset-xs-4" id="approved">
                <p>&nbsp;</p>
                <p><strong>Approved By</strong></p>
            </div>
        </div>
    </div>
    </body>
    <script src="/js/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            if($("#serviceselect").length){ $("#tag strong").text("Service: " + $("#serviceselect")[0].selectedOptions[0].text); }
            if($("#divisionselect").length){ $("#tag strong").text("Division: " + $("#divisionselect")[0].selectedOptions[0].text); }
            if($("#regionselect").length){ $("#tag strong").text("Region: " + $("#regionselect")[0].selectedOptions[0].text); }
            if($("#districtselect").length){ $("#tag strong").text("Disctrict: " + $("#districtselect")[0].selectedOptions[0].text); }
            $('#agentslist').DataTable( {
                dom: 'lBfrtip',
                "lengthMenu": [[50, 100, 500, 1000, -1], [50, 100, 500, 1000, "All"]],
                buttons: [
                {
                    extend: 'copy',
                    text: 'Copy to clipboard',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                },
                'excelHtml5',
                'csvHtml5',
                {
                    extend: 'pdfHtml5',
                    title: 'National Bureau of Investigation',
                    messageTop : $("#tag strong").text(),
                    customize: function ( doc ) {
                        doc.content.splice( 1, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'center',
                            text: 'Agent Ranking'
                        } );
                    }
                }],
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false
                
            });

            var service = $("#serviceselect option:selected").text();

            $("#service").text("Service: " + service);

            $('#min').keyup( function() { table.draw(); } );
            $('#max').keyup( function() { table.draw(); } );
            $('#pmin').keyup( function() { table.draw(); } );
            $('#pmax').keyup( function() { table.draw(); } );

            $( "#min" ).datepicker({ dateFormat: 'yy-mm-dd' }).on("show", function() {
                $(this).val("{{ $min }}").datepicker('update');
            });
            $( "#max" ).datepicker({ dateFormat: 'yy-mm-dd' }).on("show", function() {
                $(this).val("{{ $max }}").datepicker('update');
            });
            $( "#pmin" ).datepicker({ dateFormat: 'yy-mm-dd' }).on("show", function() {
                $(this).val("{{ $pmin }}").datepicker('update');
            });
            $( "#pmax" ).datepicker({ dateFormat: 'yy-mm-dd' }).on("show", function() {
                $(this).val("{{ $pmax }}").datepicker('update');
            });

            $('#submit').click(function(){
                 $( "#datefilter" ).submit();
            });

            

        } );

        
    </script>
</html>