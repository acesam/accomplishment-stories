<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'Auth\LoginController@Login');

Route::post('/login', 'Auth\LoginController@Authenticate');

Route::get('/logout', 'Auth\LoginController@Logout');

Route::middleware(['authnbi'])->group(function () {
	
	Route::get('/', 'ReportController@Index');

	Route::get('/settings/roles', 'SettingsController@Roles');

	Route::post('/settings/roles', 'SettingsController@SaveRoles');

	Route::get('/agents/all', 'ReportController@AllAgents');

	Route::post('/agents/all', 'ReportController@FilterAllAgents');

	Route::get('/agents/services', 'ReportController@ServiceAgents');

	Route::post('/agents/services', 'ReportController@FilterServiceAgents');

	Route::get('/agents/division', 'ReportController@DivisionAgents');

	Route::post('/agents/division', 'ReportController@FilterDivisionAgents');

	Route::get('/agents/region', 'ReportController@RegionAgents');

	Route::post('/agents/region', 'ReportController@FilterRegionAgents');

	Route::get('/agents/district', 'ReportController@DistrictAgents');

	Route::post('/agents/district', 'ReportController@FilterDistrictAgents');

	Route::get('/agents/cases/{id}/{type}/{min}/{max}/{agent}', 'ReportController@CurrentCases');

	Route::get('/agents/naturecaseslist/{division}/{nature}/{status}', 'ReportController@NatureOfCasesList');

	Route::get('/agents/naturecaseslist/{division}/{nature}/{status}/{service}/{category}/{min}/{max}/{role}', 'ReportController@NatureOfCasesListSingle');

	Route::get('/agents/naturecasestotal/{division}/{status}', 'ReportController@NatureOfCasesTotal');

	Route::get('/agents/naturecases/select', 'ReportController@NatureOfCasesSelector');

	Route::get('/agents/naturecases/{casenatureid}', 'ReportController@NatureOfCasesSingle');

	Route::post('/agents/naturecases/{casenatureid}', 'ReportController@FilterNatureOfCasesSingle');

	Route::get('/agents/naturecases', 'ReportController@NatureOfCases');

	Route::get('/agents/ranking/all', 'ReportController@AllRankingAgents');

	Route::post('/agents/ranking/all', 'ReportController@FilterAllRankingAgents');

	Route::get('/agents/ranking/services', 'ReportController@ServiceRankingAgents');

	Route::post('/agents/ranking/services', 'ReportController@FilterServiceRankingAgents');

	Route::get('/agents/ranking/division', 'ReportController@DivisionRankingAgents');

	Route::post('/agents/ranking/division', 'ReportController@FilterDivisionRankingAgents');

	Route::get('/agents/ranking/region', 'ReportController@RegionRankingAgents');

	Route::post('/agents/ranking/region', 'ReportController@FilterRegionRankingAgents');

	Route::get('/agents/ranking/district', 'ReportController@DistrictRankingAgents');

	Route::post('/agents/ranking/district', 'ReportController@FilterDistrictRankingAgents');
});