-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2018 at 03:26 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acmo_reportsdb`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `GENERATE_REPORTS_CASES` ()  BEGIN
TRUNCATE TABLE acmo_reportsdb.reports_cases ;
INSERT INTO 
acmo_reportsdb.reports_cases 
(`user_id`, 
`agent_type`, 
`case_id`, 
`case_status`, 
`case_category`, 
`caseservice_id`, 
`nature_id`, 
`nature`,  
`ccn`, 
`score`, 
`date_approved`)

SELECT activities.user_id, 'main' as agent_type, activities.case_id, cases.casestatus_id as case_status, cases.casecategory_id as case_category, cases.caseservice_id, casenature.casenature_id as nature_id, casenature.name as nature, cases.ccn_id as ccn, activities.score, activities.date_approved 
FROM acmo_casesdb.case_notes_agents_activities as activities 
LEFT JOIN acmo_casesdb.cases as cases ON cases.case_id = activities.case_id 
LEFT JOIN acmo_casesdb.case_natures as nature_rel ON nature_rel.case_id = activities.case_id 
LEFT JOIN acmo_settingsdb.settings_casenatures casenature ON casenature.casenature_id = nature_rel.casenature_id 
WHERE activities.status LIKE 'APPROVED'

UNION ALL 

SELECT tags.user_id, 'tag' as agent_type, tags.case_id, cases.casestatus_id as case_status, cases.casecategory_id as case_category, cases.caseservice_id, casenature.casenature_id as nature_id, casenature.name as nature, cases.ccn_id as ccn, tags.value as score, tags.date_approved 
FROM acmo_casesdb.case_notes_agents_tags as tags
LEFT JOIN acmo_casesdb.cases as cases ON cases.case_id = tags.case_id 
LEFT JOIN acmo_casesdb.case_natures as nature_rel ON nature_rel.case_id = tags.case_id 
LEFT JOIN acmo_settingsdb.settings_casenatures casenature ON casenature.casenature_id = nature_rel.casenature_id 
WHERE  tags.status LIKE 'APPROVED';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `REPORTS_AGENTS_SCORES` (IN `status` VARCHAR(50), IN `user_id` VARCHAR(50), IN `min` VARCHAR(50), IN `max` VARCHAR(50))  BEGIN
DECLARE statusquery VARCHAR(200) DEFAULT '';
if status = 'active' then
	SET statusquery = 'AND report.case_status NOT LIKE ''CS0'' AND report.case_status NOT LIKE ''CS4''';
end if;

if status = 'closed' then
	SET statusquery = 'AND report.case_status LIKE ''CS0''';
end if;

if status = 'prosecution' then
	SET statusquery = 'AND report.case_status LIKE ''CS4''';
end if;

SET @statement = CONCAT('SELECT report.ccn as ccn_id, report.case_status as casestatus_id, (SELECT date_approved from acmo_reportsdb.reports_cases where ccn LIKE report.ccn ORDER BY id ASC LIMIT 1) as date_approved, (SELECT score from acmo_reportsdb.reports_cases where ccn LIKE report.ccn ORDER BY id ASC LIMIT 1) as score FROM acmo_reportsdb.reports_cases as report WHERE date_approved BETWEEN ''',min,''' AND ''',max,''' AND report.user_id LIKE ''', user_id,''' ', statusquery, ' GROUP BY ccn, case_status');

PREPARE stmt FROM @statement;
EXECUTE stmt;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `REPORTS_AGENTS_SCORES_TAGS` (IN `status` VARCHAR(50), IN `user_id` VARCHAR(50), IN `min` VARCHAR(50), IN `max` VARCHAR(50))  BEGIN
DECLARE statusquery VARCHAR(200) DEFAULT '';
if status = 'active' then
	SET statusquery = 'AND cases.casestatus_id NOT LIKE ''CS0'' AND cases.casestatus_id NOT LIKE ''CS4''';
end if;

if status = 'closed' then
	SET statusquery = 'AND cases.casestatus_id LIKE ''CS0''';
end if;

if status = 'prosecution' then
	SET statusquery = 'AND cases.casestatus_id LIKE ''CS4''';
end if;
SET @statement = CONCAT('SELECT activities.case_id, activities.value as score, profiles.first_name, profiles.last_name, activities.date_approved, cases.ccn_id, cases.casestatus_id FROM acmo_casesdb.case_notes_agents_tags as activities LEFT JOIN acmo_casesdb.cases ON activities.case_id = cases.case_id LEFT JOIN acmo_usersdb.user_profiles as profiles ON activities.user_id = profiles.user_id WHERE status LIKE ''APPROVED'' AND activities.date_approved BETWEEN ''',min,''' AND ''',max,''' AND activities.user_id LIKE ''', user_id,''' ', statusquery);

PREPARE stmt FROM @statement;
EXECUTE stmt;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `REPORTS_ALL_AGENTS` (IN `category` VARCHAR(50), IN `min` DATE, IN `max` DATE, IN `agent_type` VARCHAR(50), IN `agent_role` VARCHAR(50), IN `service_id` VARCHAR(50), IN `division_id` VARCHAR(50), IN `region_id` VARCHAR(50), IN `district_id` VARCHAR(50))  BEGIN
DECLARE d_category VARCHAR(50) DEFAULT '%';
DECLARE d_min DATETIME DEFAULT '2016-01-01';
DECLARE d_max DATETIME DEFAULT '2018-01-01';
DECLARE d_roles VARCHAR(200) DEFAULT '%';
DECLARE d_serviceid VARCHAR(50) DEFAULT '%';
DECLARE d_divisionid VARCHAR(50) DEFAULT '%';
DECLARE d_regionid VARCHAR(50) DEFAULT '%';
DECLARE d_districtid VARCHAR(50) DEFAULT '%';
DECLARE regionquery VARCHAR(200) DEFAULT '';
if category <> '0' then
	SET d_category = category;
end if;
SET d_min = min;
SET d_max = max;
SET @d_activity = 'agent';

SET d_roles = CONCAT('(users.role_id LIKE ''123e4ab4-8fe6-44f1-a398-42f772d9a252'' OR users.role_id LIKE ''b700d769-6bf3-4628-8544-a73bc7fd322b'') ');
if agent_role <> '0' then
	SET d_roles = CONCAT('users.role_id LIKE ''',agent_role,'''');
end if;

if service_id <> '0' then
	SET d_serviceid = service_id;
end if;

if division_id <> '0' then
	SET d_divisionid = division_id;
    SET regionquery = CONCAT('AND users.division_id LIKE ''', d_divisionid,'''');
end if;

if region_id <> '0' then
	SET d_regionid = region_id;
    SET regionquery = CONCAT('AND users.region_id LIKE ''', d_regionid,''' AND users.districtoffice_id IS NULL');
end if;

if district_id <> '0' then
	SET d_districtid = district_id;
    SET regionquery = CONCAT('AND users.region_id IS NOT NULL AND users.districtoffice_id LIKE ''',d_districtid,'''');
end if;

SET @statement = CONCAT('SELECT users.user_id, users.role_id, users.caseservice_id, CONCAT(profile.first_name, '' '', profile.last_name) as full_name, users.region_id, region.name as region_name, users.division_id, division.name as division_name, users.districtoffice_id as district_id, district.name as district_name, coalesce((SELECT SUM(score) FROM acmo_reportsdb.reports_cases WHERE case_status LIKE ''CS4'' AND date_approved BETWEEN ''',d_min,''' AND ''',d_max,''' AND agent_type LIKE ''',agent_type,''' AND case_category LIKE ''',d_category,''' AND user_id LIKE users.user_id), 0) as prosecuted, coalesce((SELECT SUM(score) FROM acmo_reportsdb.reports_cases WHERE case_status LIKE ''CS0'' AND date_approved BETWEEN ''',d_min,''' AND ''',d_max,''' AND agent_type LIKE ''',agent_type,''' AND case_category LIKE ''',d_category,''' AND user_id LIKE users.user_id), 0) as closed, coalesce((SELECT SUM(score) FROM acmo_reportsdb.reports_cases WHERE case_status NOT LIKE ''CS4'' AND case_status NOT LIKE ''CS0'' AND date_approved BETWEEN ''',d_min,''' AND ''',d_max,''' AND agent_type LIKE ''',agent_type,''' AND case_category LIKE ''',d_category,''' AND user_id LIKE users.user_id), 0) as active FROM acmo_usersdb.user_accounts as users LEFT JOIN acmo_usersdb.user_profiles as profile ON users.user_id = profile.user_id LEFT JOIN acmo_settingsdb.settings_regions region ON users.region_id = region.region_id LEFT JOIN acmo_settingsdb.settings_divisions division ON users.division_id = division.division_id LEFT JOIN acmo_settingsdb.settings_districtoffices district ON users.districtoffice_id = district.districtoffice_id WHERE profile.first_name IS NOT NULL AND ',d_roles,' AND users.caseservice_id LIKE ''',d_serviceid,''' ', regionquery);
PREPARE stmt FROM @statement;
EXECUTE stmt;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `reports_cases`
--

CREATE TABLE `reports_cases` (
  `id` int(11) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `agent_type` varchar(5) DEFAULT NULL,
  `case_id` varchar(37) DEFAULT NULL,
  `case_status` varchar(7) DEFAULT NULL,
  `case_category` varchar(1) DEFAULT NULL,
  `caseservice_id` varchar(37) DEFAULT NULL,
  `nature_id` varchar(37) DEFAULT NULL,
  `nature` varchar(50) DEFAULT NULL,
  `ccn` varchar(15) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `date_approved` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports_cases`
--

INSERT INTO `reports_cases` (`id`, `user_id`, `agent_type`, `case_id`, `case_status`, `case_category`, `caseservice_id`, `nature_id`, `nature`, `ccn`, `score`, `date_approved`) VALUES
(1, 'ff9f2c37-48aa-11e8-9916-0242ac160002', 'main', 'a7eb758f-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-05131', 180, '2016-05-20 00:00:00'),
(2, 'ff9f3095-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(3, 'ff9f2ff3-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(4, 'ff9f1e61-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(5, 'ff9f2049-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(6, 'ff9f20e7-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(7, 'ff9f2374-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(8, 'ff9f2697-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(9, 'ff9f27d6-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(10, 'ff9f29b3-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(11, 'ff9f2a54-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(12, 'ff9f2af6-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(13, 'ff9f2b95-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(14, 'ff9f2c37-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(15, 'ff9f2e14-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 180, '2016-06-03 00:00:00'),
(16, 'ff9dec05-48aa-11e8-9916-0242ac160002', 'main', 'a7af3122-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-00946', 5040, '2016-05-07 00:00:00'),
(17, 'ff9e3296-48aa-11e8-9916-0242ac160002', 'main', 'a7e80f82-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'd041ba84-4bce-4439-89b1-7a6246d1afe7', 'QUALIFIED THEFT', '2016-C-04896', 70, '2018-05-08 02:32:18'),
(18, 'ff9f2e14-48aa-11e8-9916-0242ac160002', 'main', 'a7ebc8df-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a85126ca-1125-429e-867d-9f03b9a9d663', 'ILLEGAL RECRUITMENT', '2016-C-04938', 120, '2016-04-26 00:00:00'),
(19, 'ff9f3095-48aa-11e8-9916-0242ac160002', 'main', 'a7ec658b-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-06700', 240, '2016-05-30 00:00:00'),
(20, 'ff9f3095-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(21, 'ff9f2ff3-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(22, 'ff9f29b3-48aa-11e8-9916-0242ac160002', 'main', 'a7ec658b-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-06700', 240, '2016-05-30 00:00:00'),
(23, 'ff9f2b95-48aa-11e8-9916-0242ac160002', 'main', 'a7ec658b-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-06700', 240, '2016-05-30 00:00:00'),
(24, 'ff9f2c37-48aa-11e8-9916-0242ac160002', 'main', 'a7ec658b-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-06700', 240, '2016-05-30 00:00:00'),
(25, 'ff9f1e61-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(26, 'ff9f2049-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(27, 'ff9f20e7-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(28, 'ff9f2374-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(29, 'ff9f2697-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(30, 'ff9f27d6-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(31, 'ff9f29b3-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(32, 'ff9f2a54-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(33, 'ff9f2af6-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(34, 'ff9f2b95-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(35, 'ff9f2c37-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(36, 'ff9f2e14-48aa-11e8-9916-0242ac160002', 'main', 'a7ec678a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-07064', 120, '2016-06-03 00:00:00'),
(37, 'ff9e3296-48aa-11e8-9916-0242ac160002', 'main', 'a7e80f82-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'd041ba84-4bce-4439-89b1-7a6246d1afe7', 'QUALIFIED THEFT', '2016-C-04896', 10, '2018-05-16 04:14:01'),
(38, 'ff9e3493-48aa-11e8-9916-0242ac160002', 'main', 'a7e8433b-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', NULL, NULL, '2016-C-05907', 10, '2018-05-16 04:14:53'),
(39, 'ff9f2cd6-48aa-11e8-9916-0242ac160002', 'main', 'a8b932cb-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', NULL, NULL, '2016-M-07457', 60, '2016-06-02 00:00:00'),
(40, 'ff9e3493-48aa-11e8-9916-0242ac160002', 'main', 'a8b3d015-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-06389', 240, '2016-05-12 00:00:00'),
(41, 'ff9f1800-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(42, 'ff9e2f98-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(43, 'ff9e3096-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(44, 'ff9e3296-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(45, 'ff9e3397-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(46, 'ff9e3493-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(47, 'ff9e3594-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(48, 'ff9e3693-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(49, 'ff9f1abe-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(50, 'ff9f1bab-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(51, 'ff9f1c6b-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(52, 'ff9f1d18-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(53, 'ff9f1dbf-48aa-11e8-9916-0242ac160002', 'main', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(54, 'ff9de59d-48aa-11e8-9916-0242ac160002', 'main', 'a7aea00c-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-03423', 320, '2016-05-02 00:00:00'),
(55, 'ff9de2e0-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(56, 'ff9de1f6-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(57, 'ff9de3c7-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(58, 'ff9de4b3-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(59, 'ff9de59d-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(60, 'ff9f4155-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(61, 'ff9de687-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(62, 'ff9de771-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(63, 'ff9de85c-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(64, 'ff9de942-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(65, 'ff9dea2e-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(66, 'ff9deb17-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(67, 'ff9dec05-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(68, 'ff9decf1-48aa-11e8-9916-0242ac160002', 'main', 'a8088532-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-10389', 180, '2016-11-03 00:00:00'),
(69, 'ff9de2e0-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(70, 'ff9de1f6-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(71, 'ff9de3c7-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(72, 'ff9de4b3-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(73, 'ff9de59d-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(74, 'ff9f4155-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(75, 'ff9de687-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(76, 'ff9de771-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(77, 'ff9de85c-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(78, 'ff9de942-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(79, 'ff9dea2e-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(80, 'ff9deb17-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(81, 'ff9dec05-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(82, 'ff9e148a-48aa-11e8-9916-0242ac160002', 'main', 'a7b2797f-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '28c98690-f2ae-4e5d-ae7a-56059d6079f3', 'ROBBERY', '2016-C-03226', 60, '2016-07-13 00:00:00'),
(83, 'ff9f29b3-48aa-11e8-9916-0242ac160002', 'main', 'a7bb9afe-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a85126ca-1125-429e-867d-9f03b9a9d663', 'ILLEGAL RECRUITMENT', '2016-C-02324', 120, '2016-07-04 00:00:00'),
(84, 'ff9f2374-48aa-11e8-9916-0242ac160002', 'main', 'a7bb9afe-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a85126ca-1125-429e-867d-9f03b9a9d663', 'ILLEGAL RECRUITMENT', '2016-C-02324', 120, '2016-07-04 00:00:00'),
(85, 'ff9f2a54-48aa-11e8-9916-0242ac160002', 'main', 'a7bb9afe-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a85126ca-1125-429e-867d-9f03b9a9d663', 'ILLEGAL RECRUITMENT', '2016-C-02324', 120, '2016-07-04 00:00:00'),
(86, 'ff9f2b95-48aa-11e8-9916-0242ac160002', 'main', 'a7bb9afe-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a85126ca-1125-429e-867d-9f03b9a9d663', 'ILLEGAL RECRUITMENT', '2016-C-02324', 120, '2016-07-04 00:00:00'),
(87, 'ff9f2c37-48aa-11e8-9916-0242ac160002', 'main', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(88, 'ff9f1e61-48aa-11e8-9916-0242ac160002', 'main', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(89, 'ff9f2049-48aa-11e8-9916-0242ac160002', 'main', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(90, 'ff9f2374-48aa-11e8-9916-0242ac160002', 'main', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(91, 'ff9f29b3-48aa-11e8-9916-0242ac160002', 'main', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(92, 'ff9f2a54-48aa-11e8-9916-0242ac160002', 'main', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(93, 'ff9f2b95-48aa-11e8-9916-0242ac160002', 'main', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(94, 'ff9f3cf4-48aa-11e8-9916-0242ac160002', 'main', 'a7ee9981-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '9670cca6-5099-4148-a4cf-70fd935c85ae', 'ABDUCTION', '2016-C-05661', 120, '2016-08-08 00:00:00'),
(95, 'ff9e3397-48aa-11e8-9916-0242ac160002', 'main', 'a8b3b5cb-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07107', 240, '2016-07-11 00:00:00'),
(96, 'ff9d65ca-48aa-11e8-9916-0242ac160002', 'main', 'a828bff8-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-01671', 240, '2016-07-11 00:00:00'),
(97, 'ff9f64df-48aa-11e8-9916-0242ac160002', 'main', 'a8c510f4-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', NULL, NULL, '2016-M-08587', 240, '2016-07-19 00:00:00'),
(98, 'ff9de59d-48aa-11e8-9916-0242ac160002', 'main', 'a7ae9c46-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01806', 160, '2016-11-03 00:00:00'),
(99, 'ff9de2e0-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(100, 'ff9de1f6-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(101, 'ff9de3c7-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(102, 'ff9de4b3-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(103, 'ff9de59d-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(104, 'ff9f4155-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(105, 'ff9de687-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(106, 'ff9de771-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(107, 'ff9de85c-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(108, 'ff9de942-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(109, 'ff9dea2e-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(110, 'ff9deb17-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(111, 'ff9dec05-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(112, 'ff9decf1-48aa-11e8-9916-0242ac160002', 'main', 'a7ae506a-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'e833cbcc-3696-4049-81a1-81f8840d19ee', 'INTELLECTUAL PROPERTY', '2016-C-01990', 320, '2016-11-03 00:00:00'),
(113, 'ff9f2cd6-48aa-11e8-9916-0242ac160002', 'main', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(114, 'ff9f1f02-48aa-11e8-9916-0242ac160002', 'main', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(115, 'ff9f218c-48aa-11e8-9916-0242ac160002', 'main', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(116, 'ff9f2413-48aa-11e8-9916-0242ac160002', 'main', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(117, 'ff9f25f5-48aa-11e8-9916-0242ac160002', 'main', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(118, 'ff9f2d75-48aa-11e8-9916-0242ac160002', 'main', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(119, 'ff9f2eb2-48aa-11e8-9916-0242ac160002', 'main', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(120, 'ff9f2f52-48aa-11e8-9916-0242ac160002', 'main', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(121, 'ff9decf1-48aa-11e8-9916-0242ac160002', 'main', 'a7df23cc-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '929d0a14-f437-47ea-86dc-1a98ab9adced', 'CONSUMER ACT OF THE PHILS.', '2016-C-04777', 240, '2016-11-08 00:00:00'),
(122, 'ff9e148a-48aa-11e8-9916-0242ac160002', 'main', 'a8b02eaf-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-12369', 0, '2017-10-20 00:00:00'),
(123, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '1082d2bc-84ec-4440-a34d-2b1df034b12e', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', NULL, NULL, '2018-C-0073400', 469, '2018-05-16 01:35:44'),
(124, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '4d10f9e3-b733-460d-a31d-1c5e71d25313', 'CS0', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', NULL, NULL, '2018-M-0073402', 200, '2018-05-16 02:44:31'),
(125, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '4d10f9e3-b733-460d-a31d-1c5e71d25313', 'CS0', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', NULL, NULL, '2018-M-0073402', 200, '2018-05-16 02:44:46'),
(126, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'a707d2dc-7a8f-4b6e-8a17-48be0a76bdf3', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', NULL, NULL, '2018-C-0073406', 20, '2018-05-16 04:36:06'),
(127, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'f0446d45-e66d-498d-b3e9-a6f9e5217cec', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '119d4196-abda-4567-a0d3-aa940408e709', 'ABORTION', '2018-C-0073405', 400, '2018-05-21 14:43:18'),
(128, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'f0446d45-e66d-498d-b3e9-a6f9e5217cec', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '508cd8d9-8088-4be8-b588-127681a9120b', 'ANTI-CHILD PORNOGRAPHY LAW', '2018-C-0073405', 400, '2018-05-21 14:43:18'),
(129, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'f0446d45-e66d-498d-b3e9-a6f9e5217cec', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'b4a8f7fd-85f9-49fc-9573-2f2b9ecc3fab', 'ACTS OF LASCIVIOUSNESS', '2018-C-0073405', 400, '2018-05-21 14:43:18'),
(130, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'f0446d45-e66d-498d-b3e9-a6f9e5217cec', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '119d4196-abda-4567-a0d3-aa940408e709', 'ABORTION', '2018-C-0073405', 200, '2018-05-21 14:42:37'),
(131, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'f0446d45-e66d-498d-b3e9-a6f9e5217cec', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '508cd8d9-8088-4be8-b588-127681a9120b', 'ANTI-CHILD PORNOGRAPHY LAW', '2018-C-0073405', 200, '2018-05-21 14:42:37'),
(132, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'f0446d45-e66d-498d-b3e9-a6f9e5217cec', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'b4a8f7fd-85f9-49fc-9573-2f2b9ecc3fab', 'ACTS OF LASCIVIOUSNESS', '2018-C-0073405', 200, '2018-05-21 14:42:37'),
(133, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '1fe26c9a-6f55-416f-bfe1-46f1a12d7644', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '119d4196-abda-4567-a0d3-aa940408e709', 'ABORTION', '2018-C-0073410', 500, '2018-05-21 20:30:21'),
(134, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '1fe26c9a-6f55-416f-bfe1-46f1a12d7644', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'b4a8f7fd-85f9-49fc-9573-2f2b9ecc3fab', 'ACTS OF LASCIVIOUSNESS', '2018-C-0073410', 500, '2018-05-21 20:30:21'),
(135, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '1fe26c9a-6f55-416f-bfe1-46f1a12d7644', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '119d4196-abda-4567-a0d3-aa940408e709', 'ABORTION', '2018-C-0073410', 300, '2018-05-21 20:29:48'),
(136, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '1fe26c9a-6f55-416f-bfe1-46f1a12d7644', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'b4a8f7fd-85f9-49fc-9573-2f2b9ecc3fab', 'ACTS OF LASCIVIOUSNESS', '2018-C-0073410', 300, '2018-05-21 20:29:48'),
(137, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '1fe26c9a-6f55-416f-bfe1-46f1a12d7644', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '119d4196-abda-4567-a0d3-aa940408e709', 'ABORTION', '2018-C-0073410', 400, '2018-05-22 00:58:31'),
(138, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '1fe26c9a-6f55-416f-bfe1-46f1a12d7644', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'b4a8f7fd-85f9-49fc-9573-2f2b9ecc3fab', 'ACTS OF LASCIVIOUSNESS', '2018-C-0073410', 400, '2018-05-22 00:58:31'),
(139, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '44ba17aa-76b6-4c0a-b05d-e0750cf84666', 'CS100', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '5fea6134-c66a-4062-bd69-e41f4d188c39', 'ADULTERY ', '2018-C-0073413', 100, '2018-05-22 01:20:54'),
(140, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', '44ba17aa-76b6-4c0a-b05d-e0750cf84666', 'CS100', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '9670cca6-5099-4148-a4cf-70fd935c85ae', 'ABDUCTION', '2018-C-0073413', 100, '2018-05-22 01:20:54'),
(141, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'b3711fac-451e-4c85-80b9-901af06cac84', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '5c3c4b66-0d6e-4a0c-8a48-9235a3632078', 'ESTAFA/SWINDLING', '2018-C-0073414', 10, '2018-05-22 07:53:37'),
(142, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'b3711fac-451e-4c85-80b9-901af06cac84', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a220036e-3835-4816-954d-8fd8bb9eee64', 'GRAVE THREATS', '2018-C-0073414', 10, '2018-05-22 07:53:37'),
(143, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'b3711fac-451e-4c85-80b9-901af06cac84', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '5c3c4b66-0d6e-4a0c-8a48-9235a3632078', 'ESTAFA/SWINDLING', '2018-C-0073414', 20, '2018-05-22 07:54:11'),
(144, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'b3711fac-451e-4c85-80b9-901af06cac84', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a220036e-3835-4816-954d-8fd8bb9eee64', 'GRAVE THREATS', '2018-C-0073414', 20, '2018-05-22 07:54:11'),
(145, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'b3711fac-451e-4c85-80b9-901af06cac84', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '5c3c4b66-0d6e-4a0c-8a48-9235a3632078', 'ESTAFA/SWINDLING', '2018-C-0073414', 30, '2018-05-22 07:55:06'),
(146, '0171272f-dcd2-486f-8547-bc60c82b33a7', 'main', 'b3711fac-451e-4c85-80b9-901af06cac84', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a220036e-3835-4816-954d-8fd8bb9eee64', 'GRAVE THREATS', '2018-C-0073414', 30, '2018-05-22 07:55:06'),
(147, 'ff9f2374-48aa-11e8-9916-0242ac160002', 'tag', 'a7eb758f-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-05131', 180, '2016-05-20 00:00:00'),
(148, 'ff9f2a54-48aa-11e8-9916-0242ac160002', 'tag', 'a7eb758f-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-05131', 180, '2016-05-20 00:00:00'),
(149, 'ff9f2b95-48aa-11e8-9916-0242ac160002', 'tag', 'a7eb758f-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-05131', 180, '2016-05-20 00:00:00'),
(150, 'ff9f2e14-48aa-11e8-9916-0242ac160002', 'tag', 'a7eb758f-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-05131', 180, '2016-05-20 00:00:00'),
(151, 'ff9f2af6-48aa-11e8-9916-0242ac160002', 'tag', 'a7eb758f-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-05131', 180, '2016-05-20 00:00:00'),
(152, 'ff9e3693-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8433b-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', NULL, NULL, '2016-C-05907', 5, '2018-05-16 04:14:54'),
(153, 'ff9e2f98-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-24 00:00:00'),
(154, 'ff9e3096-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-24 00:00:00'),
(155, 'ff9e3296-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-24 00:00:00'),
(156, 'ff9e3397-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-24 00:00:00'),
(157, 'ff9e3493-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-24 00:00:00'),
(158, 'ff9e3594-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-24 00:00:00'),
(159, 'ff9f1abe-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-24 00:00:00'),
(160, 'ff9f1c6b-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-24 00:00:00'),
(161, 'ff9f1d18-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-24 00:00:00'),
(162, 'ff9f1dbf-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-24 00:00:00'),
(163, 'ff9e2f98-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(164, 'ff9e3096-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(165, 'ff9e3296-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(166, 'ff9e3397-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(167, 'ff9e3493-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(168, 'ff9e3594-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(169, 'ff9e3693-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(170, 'ff9f1800-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(171, 'ff9f1abe-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(172, 'ff9f1bab-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(173, 'ff9f1c6b-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(174, 'ff9f1d18-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(175, 'ff9f1dbf-48aa-11e8-9916-0242ac160002', 'tag', 'a7e8a717-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '3a6246ae-d5e6-4197-b082-f33508d152f6', 'CYBERCRIME LAW', '2016-C-04940', 480, '2016-05-30 00:00:00'),
(176, 'ff9f2374-48aa-11e8-9916-0242ac160002', 'tag', 'a7bb9afe-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a85126ca-1125-429e-867d-9f03b9a9d663', 'ILLEGAL RECRUITMENT', '2016-C-02324', 120, '2016-07-04 00:00:00'),
(177, 'ff9f2a54-48aa-11e8-9916-0242ac160002', 'tag', 'a7bb9afe-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a85126ca-1125-429e-867d-9f03b9a9d663', 'ILLEGAL RECRUITMENT', '2016-C-02324', 120, '2016-07-04 00:00:00'),
(178, 'ff9f2b95-48aa-11e8-9916-0242ac160002', 'tag', 'a7bb9afe-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a85126ca-1125-429e-867d-9f03b9a9d663', 'ILLEGAL RECRUITMENT', '2016-C-02324', 120, '2016-07-04 00:00:00'),
(179, 'ff9f1e61-48aa-11e8-9916-0242ac160002', 'tag', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(180, 'ff9f2049-48aa-11e8-9916-0242ac160002', 'tag', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(181, 'ff9f2374-48aa-11e8-9916-0242ac160002', 'tag', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(182, 'ff9f29b3-48aa-11e8-9916-0242ac160002', 'tag', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(183, 'ff9f2a54-48aa-11e8-9916-0242ac160002', 'tag', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(184, 'ff9f2b95-48aa-11e8-9916-0242ac160002', 'tag', 'a7bc0593-48b0-11e8-9916-0242ac160002', 'CS9', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '4c2858c1-dba0-40cb-b24f-c3d11847be4a', 'ANTI HUMAN TRAFFICKING LAW (RA 9208)', '2016-C-01887', 120, '2016-08-04 00:00:00'),
(185, 'ff9f1f02-48aa-11e8-9916-0242ac160002', 'tag', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(186, 'ff9f218c-48aa-11e8-9916-0242ac160002', 'tag', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(187, 'ff9f2413-48aa-11e8-9916-0242ac160002', 'tag', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(188, 'ff9f25f5-48aa-11e8-9916-0242ac160002', 'tag', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(189, 'ff9f25f5-48aa-11e8-9916-0242ac160002', 'tag', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(190, 'ff9f2d75-48aa-11e8-9916-0242ac160002', 'tag', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(191, 'ff9f2eb2-48aa-11e8-9916-0242ac160002', 'tag', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(192, 'ff9f2f52-48aa-11e8-9916-0242ac160002', 'tag', 'a8b9312c-48b0-11e8-9916-0242ac160002', 'CS9', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', '83ac998b-071a-4d5e-bbd3-9ce8612bf659', 'SERVICE OF W/A', '2016-M-07004', 0, '2016-07-01 00:00:00'),
(193, 'a163ce86-db6d-4803-8440-87ef3f570c92', 'tag', '4d10f9e3-b733-460d-a31d-1c5e71d25313', 'CS0', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', NULL, NULL, '2018-M-0073402', 200, '2018-05-16 02:44:47'),
(194, 'a163ce86-db6d-4803-8440-87ef3f570c92', 'tag', '4d10f9e3-b733-460d-a31d-1c5e71d25313', 'CS0', 'M', 'eec66846-a653-4096-98dc-15069ed55c4c', NULL, NULL, '2018-M-0073402', 200, '2018-05-16 02:44:32'),
(195, 'cb39cea4-6665-49f3-b441-8656740047f0', 'tag', 'f0446d45-e66d-498d-b3e9-a6f9e5217cec', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '119d4196-abda-4567-a0d3-aa940408e709', 'ABORTION', '2018-C-0073405', 200, '2018-05-21 14:42:37'),
(196, 'cb39cea4-6665-49f3-b441-8656740047f0', 'tag', 'f0446d45-e66d-498d-b3e9-a6f9e5217cec', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '508cd8d9-8088-4be8-b588-127681a9120b', 'ANTI-CHILD PORNOGRAPHY LAW', '2018-C-0073405', 200, '2018-05-21 14:42:37'),
(197, 'cb39cea4-6665-49f3-b441-8656740047f0', 'tag', 'f0446d45-e66d-498d-b3e9-a6f9e5217cec', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'b4a8f7fd-85f9-49fc-9573-2f2b9ecc3fab', 'ACTS OF LASCIVIOUSNESS', '2018-C-0073405', 200, '2018-05-21 14:42:37');
INSERT INTO `reports_cases` (`id`, `user_id`, `agent_type`, `case_id`, `case_status`, `case_category`, `caseservice_id`, `nature_id`, `nature`, `ccn`, `score`, `date_approved`) VALUES
(198, 'ff9e2d99-48aa-11e8-9916-0242ac160002', 'tag', '1fe26c9a-6f55-416f-bfe1-46f1a12d7644', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '119d4196-abda-4567-a0d3-aa940408e709', 'ABORTION', '2018-C-0073410', 300, '2018-05-21 20:29:49'),
(199, 'ff9e2d99-48aa-11e8-9916-0242ac160002', 'tag', '1fe26c9a-6f55-416f-bfe1-46f1a12d7644', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'b4a8f7fd-85f9-49fc-9573-2f2b9ecc3fab', 'ACTS OF LASCIVIOUSNESS', '2018-C-0073410', 300, '2018-05-21 20:29:49'),
(200, 'a163ce86-db6d-4803-8440-87ef3f570c92', 'tag', '1fe26c9a-6f55-416f-bfe1-46f1a12d7644', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '119d4196-abda-4567-a0d3-aa940408e709', 'ABORTION', '2018-C-0073410', 500, '2018-05-21 20:30:21'),
(201, 'a163ce86-db6d-4803-8440-87ef3f570c92', 'tag', '1fe26c9a-6f55-416f-bfe1-46f1a12d7644', 'CS0', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'b4a8f7fd-85f9-49fc-9573-2f2b9ecc3fab', 'ACTS OF LASCIVIOUSNESS', '2018-C-0073410', 500, '2018-05-21 20:30:21'),
(202, 'cfca2d88-5a4f-4969-8ea4-0164ad2216de', 'tag', '44ba17aa-76b6-4c0a-b05d-e0750cf84666', 'CS100', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '5fea6134-c66a-4062-bd69-e41f4d188c39', 'ADULTERY ', '2018-C-0073413', 100, '2018-05-22 01:20:55'),
(203, 'cfca2d88-5a4f-4969-8ea4-0164ad2216de', 'tag', '44ba17aa-76b6-4c0a-b05d-e0750cf84666', 'CS100', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '9670cca6-5099-4148-a4cf-70fd935c85ae', 'ABDUCTION', '2018-C-0073413', 100, '2018-05-22 01:20:55'),
(204, 'ff9e2d99-48aa-11e8-9916-0242ac160002', 'tag', 'b3711fac-451e-4c85-80b9-901af06cac84', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '5c3c4b66-0d6e-4a0c-8a48-9235a3632078', 'ESTAFA/SWINDLING', '2018-C-0073414', 10, '2018-05-22 07:53:37'),
(205, 'ff9e2d99-48aa-11e8-9916-0242ac160002', 'tag', 'b3711fac-451e-4c85-80b9-901af06cac84', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a220036e-3835-4816-954d-8fd8bb9eee64', 'GRAVE THREATS', '2018-C-0073414', 10, '2018-05-22 07:53:37'),
(206, 'a163ce86-db6d-4803-8440-87ef3f570c92', 'tag', 'b3711fac-451e-4c85-80b9-901af06cac84', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', '5c3c4b66-0d6e-4a0c-8a48-9235a3632078', 'ESTAFA/SWINDLING', '2018-C-0073414', 20, '2018-05-22 07:55:07'),
(207, 'a163ce86-db6d-4803-8440-87ef3f570c92', 'tag', 'b3711fac-451e-4c85-80b9-901af06cac84', 'CS3', 'C', 'eec66846-a653-4096-98dc-15069ed55c4c', 'a220036e-3835-4816-954d-8fd8bb9eee64', 'GRAVE THREATS', '2018-C-0073414', 20, '2018-05-22 07:55:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reports_cases`
--
ALTER TABLE `reports_cases`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reports_cases`
--
ALTER TABLE `reports_cases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
